﻿using AutoMapper;
using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using DocCare.Services.ServicesInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Services.ServicesAbstracts
{
    public class BookingAlgorithmService : BookingAlgorithmAbstract
    {
        private enum enPaymentMethod { CashOnExamine = 1, CreditCard = 2 };
        private enum enInvoiceStatus { Confirm = 1, Pending = 2, Cancelled = 3 };

        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public BookingAlgorithmService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        protected override void ChangeSlotStatus(SlotModel slotModel)
        {
            if (slotModel != null && slotModel.SlotId > 0)
            {
                Slot slotEF = this.unitOfWork.Slots.GetByID(slotModel.SlotId);
                slotEF.SlotIsBooked = true;
                this.unitOfWork.Slots.Update(slotEF);
            }
        }

        protected override bool CheckSlotAvailability(SlotModel slotModel)
        {
            return !this.unitOfWork.Slots.GetByID(slotModel.SlotId).SlotIsBooked;
        }

        protected override AppointmentModel GenerateAppointment(BookingDataModel bookingDataModel)
        {
            AppointmentModel appointmentModel = new AppointmentModel()
            {
                AppointmentBookDate = DateTime.Now,
                AppointmentDate = bookingDataModel.SelectedScheduleTiming.Day.DayDate,
                AppointmentAmount = bookingDataModel.Doctor.UserServicePrice,
                AppointmentFrom = bookingDataModel.SelectedSlot.SlotTimeFrom,
                AppointmentTo = bookingDataModel.SelectedSlot.SlotTimeTo,
                DoctorId = bookingDataModel.Doctor.UserId,
                PatientId = bookingDataModel.Patient.UserId
            };

            return appointmentModel;
        }

        protected override InvoiceModel GenerateInvoice(AppointmentModel appointment, PaymentMethodModel paymentMethod)
        {
            InvoiceModel invoiceModel = new InvoiceModel();

            invoiceModel.InvoiceInfo = "Examination Invoice";
            invoiceModel.InvoiceAmount = appointment.AppointmentAmount;
            invoiceModel.InvoiceDiscount = 0;
            invoiceModel.InvoiceTotalAmount = (invoiceModel.InvoiceAmount - invoiceModel.InvoiceDiscount);
            invoiceModel.InvoiceIssueDate = DateTime.Now;
            invoiceModel.InvoicePaidDate = paymentMethod.PaymentId == (int)enPaymentMethod.CashOnExamine ? appointment.AppointmentDate : invoiceModel.InvoiceIssueDate;
            invoiceModel.DoctorId = appointment.DoctorId;
            invoiceModel.PatientId = appointment.PatientId;
            invoiceModel.PaymentId = paymentMethod.PaymentId;
            invoiceModel.StatusId = (int)enInvoiceStatus.Pending;
            invoiceModel.InvoiceNo = "INV-00001";
            return invoiceModel;
        }

        protected override void GenerateInvoiceDetails()
        {
            throw new NotImplementedException();
        }

        protected override void ConfirmBooking(AppointmentModel appointment, InvoiceModel invoice)
        {
            Appointment appointmentEF = this.mapper.Map<Appointment>(appointment);
            appointmentEF.Invoices.Add(this.mapper.Map<Invoice>(invoice));
            this.unitOfWork.Appointments.Add(appointmentEF);
            this.unitOfWork.Complete();
        }
    }
}
