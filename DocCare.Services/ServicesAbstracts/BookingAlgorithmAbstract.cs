﻿using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Services.ServicesAbstracts
{
    public abstract class BookingAlgorithmAbstract
    {
        //Template Method
        public void ConfirmAndPayBooking(BookingDataModel bookingDataModel)
        {
            //Algorithm steps
            if (this.CheckSlotAvailability(bookingDataModel.SelectedSlot))
            {
                AppointmentModel appointment = this.GenerateAppointment(bookingDataModel);
                InvoiceModel invoice = this.GenerateInvoice(appointment, bookingDataModel.SelectedPaymentMethod);
                //this.GenerateInvoiceDetails();
                this.ChangeSlotStatus(bookingDataModel.SelectedSlot);
                this.ConfirmBooking(appointment, invoice);
            }
        }

        protected abstract bool CheckSlotAvailability(SlotModel slotModel);
        protected abstract AppointmentModel GenerateAppointment(BookingDataModel bookingDataModel);
        protected abstract void ChangeSlotStatus(SlotModel slotModel);
        protected abstract InvoiceModel GenerateInvoice(AppointmentModel appointment, PaymentMethodModel paymentMethod);
        protected abstract void GenerateInvoiceDetails();
        protected abstract void ConfirmBooking(AppointmentModel appointment, InvoiceModel invoice);
    }
}
