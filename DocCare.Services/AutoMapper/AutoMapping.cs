﻿using AutoMapper;
using DocCare.Data.Entities;
using DocCare.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Services.AutoMapper
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<Day, DayModel>().ReverseMap(); // means you want to map from Day to DayModel and reverse
            CreateMap<Hospital, HospitalModel>().ReverseMap();
            CreateMap<Specialist, SpecialistModel>().ReverseMap();
            CreateMap<User, UserModel>().ReverseMap();
            CreateMap<Appointment, AppointmentModel>().ReverseMap();
            CreateMap<Award, AwardModel>().ReverseMap();
            CreateMap<Education, EducationModel>().ReverseMap();
            CreateMap<DocServicePrice, DocServicePriceModel>().ReverseMap();
            //CreateMap<DoctorSlot, DoctorSlotModel>().ReverseMap();
            CreateMap<Experience, ExperienceModel>().ReverseMap();
            CreateMap<Favorite, FavoriteModel>().ReverseMap();
            CreateMap<Membership, MembershipModel>().ReverseMap();
            CreateMap<Registration, RegistrationModel>().ReverseMap();
            CreateMap<Service, ServiceModel>().ReverseMap();
            CreateMap<SocialMedia, SocialMediaModel>().ReverseMap();
            CreateMap<UserType, UserTypeModel>().ReverseMap();
            CreateMap<Gender, GenderModel>().ReverseMap();
            CreateMap<Invoice, InvoiceModel>().ReverseMap();
            CreateMap<InvoiceDetails, InvoiceDetailsModel>().ReverseMap();
            CreateMap<InvoiceStatus, InvoiceStatusModel>().ReverseMap();
            CreateMap<PaymentMethod, PaymentMethodModel>().ReverseMap();
            CreateMap<Review, ReviewModel>().ReverseMap();
            CreateMap<ScheduleTiming, ScheduleTimingModel>().ReverseMap();
            CreateMap<Service, ServiceModel>().ReverseMap();
            CreateMap<Slot, SlotModel>().ReverseMap();
            CreateMap<SlotDuration, SlotDurationModel>().ReverseMap();
        }
    }
}
