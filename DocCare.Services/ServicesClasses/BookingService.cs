﻿using AutoMapper;
using DocCare.Data.Interfaces;
using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using DocCare.Services.ServicesAbstracts;
using DocCare.Services.ServicesInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Services.ServicesClasses
{
    public class BookingService : IBookingService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        //private readonly IServiceUnitOfWork serviceUnitOfWork;
        private readonly IDayService dayService;

        private readonly BookingAlgorithmAbstract bookingAlgorithm;
        public BookingService(IUnitOfWork unitOfWork, IMapper mapper, IDayService dayService, 
            BookingAlgorithmAbstract bookingAlgorithm)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            //this.serviceUnitOfWork = serviceUnitOfWork;
            this.dayService = dayService;

            this.bookingAlgorithm = bookingAlgorithm;
        }


        public BookingDataModel CalculateDoctorScheduleTimingAndSlots(long DoctorId)
        {
            // '7' => you can make it dynamic
            BookingDataModel bookingData = new BookingDataModel(7);

            // Get Doctor By Id With his/her SchedulTimings List
            bookingData.Doctor = this.mapper.Map<UserModel>(this.unitOfWork.Users.GetByIdWithScheduleTiming(DoctorId));

            //If Doctor exist
            if(bookingData.Doctor.UserId > 0 && bookingData.Doctor.ScheduleTimings != null && bookingData.Doctor.ScheduleTimings.Count > 0)
            {
                if (bookingData.ScheduleTimings == null)
                    bookingData.ScheduleTimings = new List<ScheduleTimingModel>();

                for (int i = 0; i < bookingData.AvailableDaysNo; i++)
                {
                    bookingData.ScheduleTimings.Add(new ScheduleTimingModel
                    {
                        //Convert Type od DateTime to local system day
                        Day = this.dayService.ConvertDataTimeToLocalDay(DateTime.Now.AddDays(i))
                    });

                    //looping for replace new Schedule by Doctor Schedule.
                    foreach(ScheduleTimingModel schedule in bookingData.Doctor.ScheduleTimings)
                    {
                        if(schedule.Day.DayName.ToLower() == bookingData.ScheduleTimings[i].Day.DayName.ToLower())
                        {
                            schedule.Day = bookingData.ScheduleTimings[i].Day;
                            bookingData.ScheduleTimings[i] = schedule;
                        }
                    }
                }
                bookingData.Doctor.ScheduleTimings = null;
            }

            return bookingData;
        }

        public void ConfirmAndPayAppointment(BookingDataModel bookingDataModel)
        {
            this.bookingAlgorithm.ConfirmAndPayBooking(bookingDataModel);
        }
    }
}
