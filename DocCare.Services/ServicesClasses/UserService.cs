﻿using AutoMapper;
using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using DocCare.Domain.BaseClasses;
using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using DocCare.Domain.SearchModels;
using DocCare.Services.Extensions;
using DocCare.Services.ServicesInterfaces;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace DocCare.Services.ServicesClasses
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public enum userTypes { Admin = 1, Doctor = 2, Patient = 3 };

        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public bool Delete(UserModel oModel)
        {
            throw new NotImplementedException();
        }

        public bool Delete(long nID)
        {
            throw new NotImplementedException();
        }

        public List<UserModel> GetAll(bool bAddDefault, string sDefaultLabel)
        {
            throw new NotImplementedException();
        }

        public UserModel GetByID(long nID)
        {
            throw new NotImplementedException();
        }

        public UserDataModel GetInitial()
        {
            UserDataModel userDataModel = new UserDataModel();
            userDataModel.model = new UserModel();
            userDataModel.searchCrit = new UserSearch();
            userDataModel.pagerSettings = new PagerSettings();

            return userDataModel;
        }

        public UserModel LoginUser(string UserEmail, string UserPass)
        {
            UserModel user = this.mapper.Map<UserModel>(this.unitOfWork.Users.LoginUser(UserEmail, UserPass));
            if (user != null && user.UserId > 0)
            {
                user.UserToken = this.GenerateToken(user);
                user.UserPass = "";
                return user;
            }
            else
            {
                user = new UserModel();
                user.UserToken = null;
                return user;
            }
        }

        public string GenerateToken(UserModel userModel)
        {
            var key = Encoding.ASCII.GetBytes("DocCareKey-2374-OFFKDI940NG7:56753253-tyuw-5769-0921-kfirox29zoxv");

            var JWTToken = new JwtSecurityToken(
                issuer: "http://localhost:58985/",
                audience: "http://localhost:58985/",
                claims: this.GetUserClaims(userModel),
                notBefore: new DateTimeOffset(DateTime.Now).DateTime,
                expires: new DateTimeOffset(DateTime.Now.AddDays(1)).DateTime,
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                );

            return "Bearer " + new JwtSecurityTokenHandler().WriteToken(JWTToken);
        }

        private IEnumerable<Claim> GetUserClaims(UserModel userModel)
        {
            // add claims you want
            IEnumerable<Claim> claims = new List<Claim>
            {
                new Claim("USER_TYPE", userModel.UserTypeId.ToString()),
                new Claim("USER_NAME", userModel.UserFirstName + " " + userModel.UserLastName)
            };
            return claims;
        }

        public bool Save(UserModel oModel)
        {
            throw new NotImplementedException();
        }

        public UserModel GetUserDetails(long UserId)
        {
            UserModel userModel = new UserModel();
            userModel = this.mapper.Map<UserModel>(this.unitOfWork.Users.GetByIdWithRelatedData(UserId));
            //string str = userModel.AddFNameToLName();
            return userModel;
        }

        public void Search(UserDataModel userDataModel)
        {
            try
            {
                List<User> usersEF = this.unitOfWork.Users.SearchUsersWithRelatedData(u =>
                                (!string.IsNullOrEmpty(userDataModel.searchCrit.UserName) ? (u.UserFirstName.Contains(userDataModel.searchCrit.UserName) || u.UserLastName.Contains(userDataModel.searchCrit.UserName)) : true) &&
                                (!string.IsNullOrEmpty(userDataModel.searchCrit.Hospital) ? u.Hospital.HospitalName.Contains(userDataModel.searchCrit.Hospital) : true) &&
                                (userDataModel.searchCrit.UserType > 0 ? u.UserTypeId == userDataModel.searchCrit.UserType : true));

                if (usersEF != null && usersEF.Count > 0)
                {
                    userDataModel.pagerSettings.TotalRecords = usersEF.Count();
                    usersEF = usersEF.Skip((userDataModel.pagerSettings.CurrentPage - 1) * userDataModel.pagerSettings.SelectedRecsPerPage).Take(userDataModel.pagerSettings.SelectedRecsPerPage).ToList();
                    userDataModel.searchResults = this.mapper.Map<List<UserModel>>(usersEF);
                    userDataModel.pagerSettings.PagesCount = (int)decimal.Ceiling((decimal)userDataModel.pagerSettings.TotalRecords / (decimal)userDataModel.pagerSettings.SelectedRecsPerPage);
                }
                else
                {
                    userDataModel.searchResults = new List<UserModel>();
                    userDataModel.pagerSettings.PagesCount = 1;
                    userDataModel.pagerSettings.TotalRecords = 0;
                }
                userDataModel.pagerSettings.FillPages();

                userDataModel.opResultCode = BaseModel<UserModel, UserSearch>.enOpResultCode.Success;
                userDataModel.opResultMsg = "";

                usersEF = null;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}
