﻿using DocCare.Services.ServicesInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Services.ServicesClasses
{
    public class ServiceUnitOfWork : IServiceUnitOfWork
    {
        //public ServiceUnitOfWork(IBookingService bookingService, IDayService dayService,
        //    IHospitalService hospitalService, ISpecialistService specialistService,
        //    IUserService userService)
        //{
        //    //this.AppointmentService = appointmentService;
        //    this.BookingService = bookingService;
        //    this.DayService = dayService;
        //    this.HospitalService = hospitalService;
        //    this.SpecialistService = specialistService;
        //    this.UserService = userService;
        //}
        //public IAppointmentService AppointmentService { get; private set; }

        public IBookingService BookingService { get; private set; }

        public IDayService DayService { get; private set; }

        public IHospitalService HospitalService { get; private set; }

        public ISpecialistService SpecialistService { get; private set; }

        public IUserService UserService { get; private set; }
    }
}
