﻿using AutoMapper;
using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using DocCare.Domain.BaseClasses;
using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using DocCare.Domain.SearchModels;
using DocCare.Services.ServicesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCare.Services.ServicesClasses
{
    public class HospitalService : IHospitalService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public HospitalService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public bool Delete(HospitalModel oModel)
        {
            Hospital hospitalEF = this.mapper.Map<Hospital>(oModel);
            this.unitOfWork.Hospitals.Remove(hospitalEF);
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Delete(long nID)
        {
            Hospital hospitalEF = this.mapper.Map<Hospital>(this.GetByID(nID));
            this.unitOfWork.Hospitals.Remove(hospitalEF);
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<HospitalModel> GetAll(bool bAddDefault, string sDefaultLabel)
        {
            List<HospitalModel> hospitals = this.mapper.Map<List<HospitalModel>>(this.unitOfWork.Hospitals.GetAll());
            
            if (hospitals != null && hospitals.Count > 0)
            {
                if (bAddDefault)
                {
                    hospitals.Insert(0, new HospitalModel
                    {
                        HospitalId = 0,
                        HospitalName = sDefaultLabel
                    });
                }
                return hospitals;
            }
            else
                return new List<HospitalModel>();
        }

        public HospitalModel GetByID(long nID)
        {
            HospitalModel hospital = this.mapper.Map<HospitalModel>(this.unitOfWork.Hospitals.GetByID(nID));
            if (hospital.HospitalId > 0)
                return hospital;
            else
                return new HospitalModel();
        }

        public HospitalDataModel GetInitial()
        {
            HospitalDataModel hospitalDataModel = new HospitalDataModel();
            hospitalDataModel.model = new HospitalModel();
            hospitalDataModel.searchCrit = new HospitalsSearch();
            hospitalDataModel.pagerSettings = new PagerSettings();
            this.Search(hospitalDataModel);
            return hospitalDataModel;
        }

        public bool Save(HospitalModel oModel)
        {
            if (oModel.HospitalId > 0)
            {
                return this.Update(oModel);
            }
            else
            {
                return this.Insert(oModel);
            }
        }

        public void Search(HospitalDataModel hospitalDataModel)
        {
            List<Hospital> hospitalsEF = this.unitOfWork.Hospitals.Search(h => !string.IsNullOrEmpty(hospitalDataModel.searchCrit.HospitalName) ? h.HospitalName.Contains(hospitalDataModel.searchCrit.HospitalName) : true
                                                                        && !string.IsNullOrEmpty(hospitalDataModel.searchCrit.HospitalPhone) ? h.HospitalPhone1.Contains(hospitalDataModel.searchCrit.HospitalPhone) || h.HospitalPhone2.Contains(hospitalDataModel.searchCrit.HospitalPhone) : true
                                                                        && !string.IsNullOrEmpty(hospitalDataModel.searchCrit.HospitalEmail) ? h.HospitalEmail.Contains(hospitalDataModel.searchCrit.HospitalEmail) : true
                                                                        && hospitalDataModel.searchCrit.IsActive != -1 ? h.HospitalIsActive == Convert.ToBoolean(hospitalDataModel.searchCrit.IsActive) : true);

            if (hospitalsEF != null && hospitalsEF.Count > 0)
            {

                hospitalDataModel.pagerSettings.TotalRecords = hospitalsEF.Count();
                hospitalsEF = hospitalsEF.Skip((hospitalDataModel.pagerSettings.CurrentPage - 1) * hospitalDataModel.pagerSettings.SelectedRecsPerPage).Take(hospitalDataModel.pagerSettings.SelectedRecsPerPage).ToList();
                hospitalDataModel.searchCrit.SearchResults = this.mapper.Map<List<HospitalModel>>(hospitalsEF);
                hospitalDataModel.pagerSettings.PagesCount = (int)decimal.Ceiling((decimal)hospitalDataModel.pagerSettings.TotalRecords / (decimal)hospitalDataModel.pagerSettings.SelectedRecsPerPage);
            }
            else
            {
                hospitalDataModel.pagerSettings.PagesCount = 1;
                hospitalDataModel.pagerSettings.TotalRecords = 0;
            }
            hospitalDataModel.pagerSettings.FillPages();

            hospitalDataModel.opResultCode = BaseModel<HospitalModel, HospitalsSearch>.enOpResultCode.Success;
            hospitalDataModel.opResultMsg = "";

            hospitalsEF = null;
        }

        private bool Insert(HospitalModel oModel)
        {
            this.unitOfWork.Hospitals.Add(this.mapper.Map<Hospital>(oModel));
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool Update(HospitalModel oModel)
        {
            this.unitOfWork.Hospitals.Update(this.mapper.Map<Hospital>(oModel));
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
