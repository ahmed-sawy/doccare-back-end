﻿using AutoMapper;
using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using DocCare.Services.ServicesInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Services.ServicesClasses
{
    public class SlotService : ISlotService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public SlotService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public bool Delete(SlotModel oModel)
        {
            throw new NotImplementedException();
        }

        public bool Delete(long nID)
        {
            throw new NotImplementedException();
        }

        public List<SlotModel> GetAll(bool bAddDefault, string sDefaultLabel)
        {
            throw new NotImplementedException();
        }

        public SlotModel GetByID(long nID)
        {
            SlotModel slotModel = this.mapper.Map<SlotModel>(this.unitOfWork.Slots.GetByID(nID));
            if(slotModel != null && slotModel.SlotId > 0)
            {
                return slotModel;
            }
            return null;
        }

        public SlotDataModel GetInitial()
        {
            throw new NotImplementedException();
        }

        public bool Save(SlotModel oModel)
        {
            if (oModel.SlotId > 0)
            {
                return this.Update(oModel);
            }
            else
            {
                return this.Insert(oModel);
            }
        }

        private bool Insert(SlotModel oModel)
        {
            this.unitOfWork.Slots.Add(this.mapper.Map<Slot>(oModel));
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool Update(SlotModel oModel)
        {
            this.unitOfWork.Slots.Update(this.mapper.Map<Slot>(oModel));
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Search(SlotDataModel dataModel)
        {
            throw new NotImplementedException();
        }

        public bool IsSlotAvailable(long slotId)
        {
            return !this.unitOfWork.Slots.GetByID(slotId).SlotIsBooked;
        }

        public void ChangeSlotStatusToBooked(SlotModel slotModel)
        {
            if(slotModel != null && slotModel.SlotId > 0)
            {
                slotModel.SlotIsBooked = true;
                //this.unitOfWork.Slots.Update(this.mapper.Map<Slot>(slotModel));
                this.Save(slotModel);
            }
        }

    }
}
