﻿using AutoMapper;
using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using DocCare.Domain.BaseClasses;
using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using DocCare.Domain.SearchModels;
using DocCare.Services.ServicesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DocCare.Services.ServicesClasses
{
    public class DayService : IDayService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public DayService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public DayModel ConvertDataTimeToLocalDay(DateTime date)
        {
            DayModel dayModel = new DayModel();
            dayModel.DayName = date.DayOfWeek.ToString();
            dayModel.DayDate = date;
            return dayModel;
        }

        public bool Delete(DayModel oModel)
        {
            this.unitOfWork.Days.Remove(this.mapper.Map<DayModel, Day>(oModel));
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Delete(long nID)
        {
            DayModel oDay = this.GetByID(nID);
            this.unitOfWork.Days.Remove(this.mapper.Map<Day>(oDay));
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<DayModel> GetAll(bool bAddDefault, string sDefaultLabel)
        {
            List<Day> daysEF = this.unitOfWork.Days.GetAll();
            List<DayModel> days = this.mapper.Map<List<Day>, List<DayModel>>(daysEF);
            
            if (days != null && days.Count > 0)
            {
                if (!bAddDefault)
                    return days;
                else
                {
                    days.Insert(0, new DayModel()
                    {
                        DayId = 0,
                        DayName = sDefaultLabel
                    });
                    return days;
                }
            }
            else
                return new List<DayModel>();
        }

        public DayModel GetByID(long nID)
        {
            DayModel day = this.mapper.Map<DayModel>(this.unitOfWork.Days.GetByID(nID));
            if (day.DayId > 0)
                return day;
            else
                return new DayModel();
        }

        public DayDataModel GetInitial()
        {
            DayDataModel dataModel = new DayDataModel();
            dataModel.searchCrit = new DaysSearch();
            dataModel.pagerSettings = new PagerSettings();
            dataModel.model = new DayModel();

            this.Search(dataModel);

            return dataModel;
        }

        public bool Save(DayModel oModel)
        {
            if (oModel.DayId > 0)
            {
                return this.Update(oModel);
            }
            else
            {
                return this.Insert(oModel);
            }
        }

        public void Search(DayDataModel dayDataModel)
        {
            List<Day> daysEF = this.unitOfWork.Days.Search(d => !string.IsNullOrEmpty(dayDataModel.searchCrit.DayName) ? d.DayName.Contains(dayDataModel.searchCrit.DayName) : true
                                        && dayDataModel.searchCrit.Status != -1 ? d.DayIsActive == Convert.ToBoolean(dayDataModel.searchCrit.Status) : true);

            if (daysEF != null && daysEF.Count > 0)
            {
                dayDataModel.pagerSettings.TotalRecords = daysEF.Count();
                daysEF = daysEF.Skip((dayDataModel.pagerSettings.CurrentPage - 1) * dayDataModel.pagerSettings.SelectedRecsPerPage).Take(dayDataModel.pagerSettings.SelectedRecsPerPage).ToList();
                dayDataModel.searchResults = this.mapper.Map<List<DayModel>>(daysEF);
                dayDataModel.pagerSettings.PagesCount = (int)decimal.Ceiling((decimal)dayDataModel.pagerSettings.TotalRecords / (decimal)dayDataModel.pagerSettings.SelectedRecsPerPage);
            }
            else
            {
                dayDataModel.pagerSettings.PagesCount = 1;
                dayDataModel.pagerSettings.TotalRecords = 0;
            }
            dayDataModel.pagerSettings.FillPages();

            dayDataModel.opResultCode = BaseModel<DayModel, DaysSearch>.enOpResultCode.Success;
            dayDataModel.opResultMsg = "";

            daysEF = null;
        }

        private bool Insert(DayModel oModel)
        {
            this.unitOfWork.Days.Add(this.mapper.Map<Day>(oModel));
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool Update(DayModel oModel)
        {
            //Day day = this.unitOfWork.Days.GetByID(oModel.DayId);
            this.unitOfWork.Days.Update(this.mapper.Map<Day>(oModel));
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
