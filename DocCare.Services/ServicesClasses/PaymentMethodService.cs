﻿using AutoMapper;
using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using DocCare.Services.ServicesInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Services.ServicesClasses
{
    public class PaymentMethodService : IPaymentMethodService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public PaymentMethodService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public bool Delete(PaymentMethodModel oModel)
        {
            throw new NotImplementedException();
        }

        public bool Delete(long nID)
        {
            throw new NotImplementedException();
        }

        public List<PaymentMethodModel> GetAll(bool bAddDefault, string sDefaultLabel)
        {
            throw new NotImplementedException();
        }

        public PaymentMethodModel GetByID(long nID)
        {
            throw new NotImplementedException();
        }

        public List<PaymentMethodModel> GetDoctorPaymentMethods(long DoctorId)
        {
            List<PaymentMethodModel> paymentMethods = new List<PaymentMethodModel>();

            ICollection<HospitalPaymentMethod> paymentsEf = this.unitOfWork.Hospitals.GetHospitalDetails(this.unitOfWork.Users.GetByID(DoctorId).HospitalId).HospitalPaymentMethods;

            foreach(HospitalPaymentMethod hospitalPaymentMethod in paymentsEf)
            {
                paymentMethods.Add(this.mapper.Map<PaymentMethodModel>(hospitalPaymentMethod.PaymentMethod));
            }

            if (paymentMethods != null)
                return paymentMethods;
            else
                return null;
        }

        public PaymentMethodDataModel GetInitial()
        {
            throw new NotImplementedException();
        }

        public bool Save(PaymentMethodModel oModel)
        {
            throw new NotImplementedException();
        }

        public void Search(PaymentMethodDataModel dataModel)
        {
            throw new NotImplementedException();
        }
    }
}
