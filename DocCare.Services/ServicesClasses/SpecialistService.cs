﻿using AutoMapper;
using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using DocCare.Domain.BaseClasses;
using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using DocCare.Domain.SearchModels;
using DocCare.Services.ServicesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCare.Services.ServicesClasses
{
    public class SpecialistService : ISpecialistService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public SpecialistService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public bool Delete(SpecialistModel oModel)
        {
            this.unitOfWork.Specialists.Remove(this.mapper.Map<Specialist>(oModel));
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Delete(long nID)
        {
            SpecialistModel specialist = this.GetByID(nID);
            this.unitOfWork.Specialists.Remove(this.mapper.Map<Specialist>(specialist));
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<SpecialistModel> GetAll(bool bAddDefault, string sDefaultLabel)
        {
            List<SpecialistModel> specialists = this.mapper.Map<List<SpecialistModel>>(this.unitOfWork.Specialists.GetAll());
            
            if (specialists != null && specialists.Count > 0)
            {
                if (bAddDefault)
                {
                    specialists.Insert(0, new SpecialistModel
                    {
                        SpecialistId = 0,
                        SpecialistName = sDefaultLabel
                    });
                }
                return specialists;
            }
            else
                return new List<SpecialistModel>();
        }

        public SpecialistModel GetByID(long nID)
        {
            SpecialistModel specialist = this.mapper.Map<SpecialistModel>(this.unitOfWork.Specialists.GetByID(nID));
            if (specialist.SpecialistId > 0)
                return specialist;
            else
                return new SpecialistModel();
        }

        public SpecialistDataModel GetInitial()
        {
            SpecialistDataModel specialistDataModel = new SpecialistDataModel();
            specialistDataModel.model = new SpecialistModel();
            specialistDataModel.searchCrit = new SpecialistsSearch();
            specialistDataModel.pagerSettings = new PagerSettings();
            this.Search(specialistDataModel);
            return specialistDataModel;
        }

        public bool Save(SpecialistModel oModel)
        {
            if (oModel.SpecialistId > 0)
            {
                return this.Update(oModel);
            }
            else
            {
                return this.Insert(oModel);
            }
        }

        public void Search(SpecialistDataModel specialistDataModel)
        {
            List<Specialist> specialistsEF = this.unitOfWork.Specialists.Search(s => !string.IsNullOrEmpty(specialistDataModel.searchCrit.SpecialistName) ? s.SpecialistName == specialistDataModel.searchCrit.SpecialistName : true
                                                                            && specialistDataModel.searchCrit.IsActive != -1 ? s.SpecialistIsActive == Convert.ToBoolean(specialistDataModel.searchCrit.IsActive) : true
                                                                            && (specialistDataModel.searchCrit.Hospital != null && specialistDataModel.searchCrit.Hospital.HospitalId > 0) ? s.HospitalId == specialistDataModel.searchCrit.Hospital.HospitalId : true);

            if (specialistsEF != null && specialistsEF.Count > 0)
            {
                specialistDataModel.pagerSettings.TotalRecords = specialistsEF.Count();
                specialistsEF = specialistsEF.Skip((specialistDataModel.pagerSettings.CurrentPage - 1) * specialistDataModel.pagerSettings.SelectedRecsPerPage).Take(specialistDataModel.pagerSettings.SelectedRecsPerPage).ToList();
                specialistDataModel.searchCrit.SearchResults = this.mapper.Map<List<SpecialistModel>>(specialistsEF);
                specialistDataModel.pagerSettings.PagesCount = (int)decimal.Ceiling((decimal)specialistDataModel.pagerSettings.TotalRecords / (decimal)specialistDataModel.pagerSettings.SelectedRecsPerPage);
            }
            else
            {
                specialistDataModel.pagerSettings.PagesCount = 1;
                specialistDataModel.pagerSettings.TotalRecords = 0;
            }
            specialistDataModel.pagerSettings.FillPages();

            specialistDataModel.opResultCode = BaseModel<SpecialistModel, SpecialistsSearch>.enOpResultCode.Success;
            specialistDataModel.opResultMsg = "";

            specialistsEF = null;
        }

        private bool Insert(SpecialistModel oModel)
        {
            this.unitOfWork.Specialists.Add(this.mapper.Map<Specialist>(oModel));
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool Update(SpecialistModel oModel)
        {
            this.unitOfWork.Specialists.Update(this.mapper.Map<Specialist>(oModel));
            try
            {
                this.unitOfWork.Complete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
