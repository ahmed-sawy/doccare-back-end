﻿using DocCare.Domain.Models;

namespace DocCare.Services.Extensions
{
    public static class UserExtensions
    {
        public static string AddFNameToLName(this UserModel user)
        {
            if (user != null && !string.IsNullOrEmpty(user.UserFirstName) && !string.IsNullOrEmpty(user.UserLastName))
                return user.UserFirstName + " " + user.UserLastName;

            return "";
        }
    }
}
