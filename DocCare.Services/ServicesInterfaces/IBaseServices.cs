﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Services.ServicesInterfaces
{
    public interface IBaseServices<DataModel, Model> where DataModel : class
    {
        DataModel GetInitial();
        bool Save(Model oModel);
        bool Delete(Model oModel);
        bool Delete(long nID);
        Model GetByID(long nID);
        List<Model> GetAll(bool bAddDefault, string sDefaultLabel);
        void Search(DataModel dataModel);
    }
}
