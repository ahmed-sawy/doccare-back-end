﻿using DocCare.Domain.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Services.ServicesInterfaces
{
    public interface IBookingService
    {
        BookingDataModel CalculateDoctorScheduleTimingAndSlots(long DoctorId);

        void ConfirmAndPayAppointment(BookingDataModel bookingDataModel);
    }
}
