﻿using DocCare.Domain.DataModels;
using DocCare.Domain.Models;

namespace DocCare.Services.ServicesInterfaces
{
    public interface IHospitalService : IBaseServices<HospitalDataModel, HospitalModel>
    {
    }
}
