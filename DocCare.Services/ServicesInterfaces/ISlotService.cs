﻿using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Services.ServicesInterfaces
{
    public interface ISlotService : IBaseServices<SlotDataModel, SlotModel>
    {

        //bool ChangeSlotStatusToNotBooked(long SlotId);
        void ChangeSlotStatusToBooked(SlotModel slotModel);
        bool IsSlotAvailable(long slotId);
    }
}
