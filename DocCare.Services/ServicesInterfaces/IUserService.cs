﻿using DocCare.Domain.DataModels;
using DocCare.Domain.Models;

namespace DocCare.Services.ServicesInterfaces
{
    public interface IUserService : IBaseServices<UserDataModel, UserModel>
    {
        UserModel LoginUser(string UserEmail, string UserPass);
        UserModel GetUserDetails(long UserId);
        string GenerateToken(UserModel userModel);
    }
}
