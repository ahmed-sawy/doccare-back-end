﻿using DocCare.Domain.DataModels;
using DocCare.Domain.Models;

namespace DocCare.Services.ServicesInterfaces
{
    public interface ISpecialistService : IBaseServices<SpecialistDataModel, SpecialistModel>
    {
    }
}
