﻿using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Services.ServicesInterfaces
{
    public interface IPaymentMethodService : IBaseServices<PaymentMethodDataModel, PaymentMethodModel>
    {
        List<PaymentMethodModel> GetDoctorPaymentMethods(long DoctorId);
    }
}
