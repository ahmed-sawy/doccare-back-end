﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Services.ServicesInterfaces
{
    public interface IServiceUnitOfWork
    {
        //IAppointmentService AppointmentService { get; }
        IBookingService BookingService { get; }
        IDayService DayService { get; }
        IHospitalService HospitalService { get; }
        ISpecialistService SpecialistService { get; }
        IUserService UserService { get; }

    }
}
