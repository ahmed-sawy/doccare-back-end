﻿using DocCare.Domain.DataModels;
using DocCare.Domain.Models;

namespace DocCare.Services.ServicesInterfaces
{
    public interface IAppointmentService : IBaseServices<AppointmentDataModel,AppointmentModel>
    {
        //long GenerateAppointment();
    }
}
