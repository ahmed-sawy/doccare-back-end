﻿using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using System;

namespace DocCare.Services.ServicesInterfaces
{
    public interface IDayService : IBaseServices<DayDataModel, DayModel>
    {
        DayModel ConvertDataTimeToLocalDay(DateTime data);
    }
}
