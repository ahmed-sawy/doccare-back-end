﻿using AutoMapper;
using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using DocCare.Domain.Models;
using DocCare.Services.ServicesClasses;
using DocCare.Services.ServicesInterfaces;
using DocCare.Tests.TestData;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Tests
{
    [TestFixture]
    public class HospitalServiceShould
    {
        private readonly IHospitalService hospitalService;
        private readonly Mock<IUnitOfWork> unitOfWorkMock = new Mock<IUnitOfWork>();
        private readonly Mock<IMapper> mapperMock = new Mock<IMapper>();
        
        public HospitalServiceShould()
        {
            this.hospitalService = new HospitalService(unitOfWorkMock.Object, mapperMock.Object);
        }

        [Test]
        public void Delete_Should_Remove_Hospital_When_Hospital_Exist()
        {
            //Arrange
            var hospital = new Hospital
            {
                HospitalId = 1,
                HospitalName = "Hospitalll"
            };
            var hospitalModel = new HospitalModel
            {
                HospitalId = 1,
                HospitalName = "Hospitalll"
            };

            unitOfWorkMock.Setup(u => u.Hospitals.Remove(hospital));
            unitOfWorkMock.Setup(u => u.Complete()).Returns(1);
            mapperMock.Setup(m => m.Map<Hospital>(hospitalModel)).Returns(hospital);

            //Act
            bool isDeleted = this.hospitalService.Delete(hospitalModel);

            //Assert
            Assert.IsTrue(isDeleted);
        }

        [Test]
        public void Delete_Should_Remove_Hospital_When_Hospital_Exist_By_Id()
        {
            //Arrange
            long hospitalId = 1;
            var hospital = new Hospital
            {
                HospitalId = 1,
                HospitalName = "Hospitalll"
            };
            var hospitalModel = new HospitalModel
            {
                HospitalId = 1,
                HospitalName = "Hospitalll"
            };

            unitOfWorkMock.Setup(u => u.Hospitals.Remove(hospital));
            unitOfWorkMock.Setup(u => u.Hospitals.GetByID(hospitalId)).Returns(hospital);
            unitOfWorkMock.Setup(u => u.Complete()).Returns(1);
            mapperMock.Setup(m => m.Map<Hospital>(hospitalModel)).Returns(hospital);
            mapperMock.Setup(m => m.Map<HospitalModel>(hospital)).Returns(hospitalModel);

            //Act
            bool isDeleted = this.hospitalService.Delete(hospitalId);

            //Assert
            Assert.IsTrue(isDeleted);
        }

        [Test]
        [TestCaseSource(typeof(HospitalServiceTestData), "GetAllTestCases")]
        public void GetAll_Should_Retrive_All_Existing_Hospitals(bool addDefault,
                                                           string defaultValue,
                                                           int expectedCount)
        {
            //Arrange
            var hospitalsList = new List<Hospital>();
            hospitalsList.Add(new Hospital { HospitalId = 1, HospitalName = "hospital 1" });
            hospitalsList.Add(new Hospital { HospitalId = 2, HospitalName = "hospital 2" });

            var hospitalsModelList = new List<HospitalModel>();
            hospitalsModelList.Add(new HospitalModel { HospitalId = 1, HospitalName = "hospital 1" });
            hospitalsModelList.Add(new HospitalModel { HospitalId = 2, HospitalName = "hospital 2" });

            this.unitOfWorkMock.Setup(u => u.Hospitals.GetAll()).Returns(hospitalsList);
            this.mapperMock.Setup(m => m.Map<List<HospitalModel>>(hospitalsList)).Returns(hospitalsModelList);

            //Act
            List<HospitalModel> results = this.hospitalService.GetAll(addDefault, defaultValue);

            //Assert
            Assert.IsTrue(results.Count == expectedCount);
        }
    }
}
