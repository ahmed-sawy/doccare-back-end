﻿using AutoMapper;
using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using DocCare.Domain.Models;
using DocCare.Services.ServicesClasses;
using DocCare.Services.ServicesInterfaces;
using Moq;
using NUnit.Framework;

namespace DocCare.Tests
{
    [TestFixture]
    public class UserServiceShould
    {
        private readonly IUserService userService;
        private readonly Mock<IUnitOfWork> unitOfWorkMock = new Mock<IUnitOfWork>();
        private readonly Mock<IMapper> mapperMock = new Mock<IMapper>();

        public UserServiceShould()
        {
            this.userService = new UserService(unitOfWorkMock.Object, mapperMock.Object);
        }

        [Test]
        public void Return_Init_Object()
        {
            //Arrange

            //Act
            var user = this.userService.GetInitial();
            //Assert
            Assert.IsNotNull(user);
        }

        [Test]
        public void Return_User_Details_When_Fount()
        {
            //Arrange
            long userId = 1;
            var userDto = new User
            {
                UserId = 1,
                UserFirstName = "Ahmed"
            };
            var userDtoModel = new UserModel
            {
                UserId = 1,
                UserFirstName = "Ahmed"
            };

            mapperMock.Setup(m => m.Map<UserModel>(userDto)).Returns(userDtoModel);
            unitOfWorkMock.Setup(u => u.Users.GetByIdWithRelatedData(userId)).Returns(userDto);

            //Act
            var user = this.userService.GetUserDetails(userId);

            //Assert
            Assert.AreEqual(user.UserId, userId);
        }

        [Test]
        public void LoginUser_Should_Return_Token_When_User_Exist()
        {
            //Arrange
            string email = "ahmed@gmail.com";
            string pass = "123456";

            var userDto = new User
            {
                UserId = 1,
                UserFirstName = "Ahmed"
            };
            var userDtoModel = new UserModel
            {
                UserId = 2,
                UserFirstName = "Ahmed",
                UserLastName = "Elsawy",
                UserTypeId = 3
            };

            mapperMock.Setup(m => m.Map<UserModel>(userDto)).Returns(userDtoModel);
            unitOfWorkMock.Setup(u => u.Users.LoginUser(email, pass)).Returns(userDto);

            //Act
            var loginUser = this.userService.LoginUser(email, pass);

            //Assert
            Assert.IsNotNull(loginUser.UserToken);
        }
    }
}
