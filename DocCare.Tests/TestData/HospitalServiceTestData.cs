﻿using NUnit.Framework;
using System.Collections;

namespace DocCare.Tests.TestData
{
    public class HospitalServiceTestData
    {
        public static IEnumerable GetAllTestCases
        {
            get
            {
                yield return new TestCaseData(false, "", 2);
                yield return new TestCaseData(true, "Select", 3);
            }
        }
    }
}
