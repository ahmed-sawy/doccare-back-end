﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocCare.Services.ServicesInterfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DocCare.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HospitalController : ControllerBase
    {
        private readonly IHospitalService hospitalService;
        public HospitalController(IHospitalService hospitalService)
        {
            this.hospitalService = hospitalService;
        }


        [HttpGet]
        public ActionResult GetHospitals()
        {
            return Ok(this.hospitalService.GetInitial());
        }
    }
}