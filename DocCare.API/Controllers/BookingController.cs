﻿using DocCare.Domain.DataModels;
using DocCare.Services.ServicesInterfaces;
using Microsoft.AspNetCore.Mvc;

namespace DocCare.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        //private readonly IServiceUnitOfWork serviceUnitOfWork;
        private readonly IBookingService bookingService;
        public BookingController(IBookingService bookingService)
        {
            //this.serviceUnitOfWork = serviceUnitOfWork;
            this.bookingService = bookingService;
        }

        [HttpGet]
        public ObjectResult GetDoctorScheduleTimings(long DoctorId)
        {
            BookingDataModel bookingData = this.bookingService.CalculateDoctorScheduleTimingAndSlots(DoctorId);

            if (bookingData != null)
                return Ok(bookingData);
            else
                return BadRequest(bookingData);
        }

        [HttpPost]
        public ObjectResult ConfirmAndPay(BookingDataModel bookingDataModel)
        {
            this.bookingService.ConfirmAndPayAppointment(bookingDataModel);
            return Ok(true);
        }
    }
}