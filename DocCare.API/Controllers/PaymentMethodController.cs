﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocCare.Services.ServicesInterfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DocCare.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PaymentMethodController : ControllerBase
    {
        private readonly IPaymentMethodService paymentMethodService;
        public PaymentMethodController(IPaymentMethodService paymentMethodService)
        {
            this.paymentMethodService = paymentMethodService;
        }

        [HttpGet]
        public ObjectResult GetDoctorPaymentMethods(long doctorId)
        {
            return Ok(this.paymentMethodService.GetDoctorPaymentMethods(doctorId));
        }
    }
}