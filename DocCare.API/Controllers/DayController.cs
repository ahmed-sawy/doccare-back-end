﻿
using DocCare.Domain.Models;
using DocCare.Services.ServicesInterfaces;
using Microsoft.AspNetCore.Mvc;

namespace DocCare.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DayController : ControllerBase
    {
        private readonly IDayService dayService;
        public DayController(IDayService dayService)
        {
            this.dayService = dayService;
        }


        [HttpGet]
        public ObjectResult GetInitObject()
        {
            return Ok(this.dayService.GetInitial());
        }

        [HttpPost]
        public ObjectResult AddDay(DayModel day)
        {
            if (this.dayService.Save(day))
            {
                return Ok(this.dayService.GetInitial());
            }
            else
            {
                return BadRequest(this.dayService.GetInitial());
            }
        }
    }
}