﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DocCare.Services.ServicesInterfaces;

namespace DocCare.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SpecialistController : ControllerBase
    {
        private readonly ISpecialistService specialistService;
        public SpecialistController(ISpecialistService specialistService)
        {
            this.specialistService = specialistService;
        }

        [HttpGet]
        public ActionResult GetSpecialists()
        {
            return Ok(this.specialistService.GetInitial());
        }
    }
}