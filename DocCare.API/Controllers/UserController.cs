﻿using DocCare.Domain.DataModels;
using DocCare.Domain.Models;
using DocCare.Services.ServicesInterfaces;
using Microsoft.AspNetCore.Mvc;

namespace DocCare.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        public UserController(IUserService userService)
        {
            this.userService = userService;
        }


        [HttpGet]
        public ObjectResult GetInitObject()
        {
            return Ok(this.userService.GetInitial());
        }

        [HttpGet]
        public ObjectResult GetUserDetails(long userId)
        {
            UserModel userModel = this.userService.GetUserDetails(userId);
            if (userModel != null && userModel.UserId > 0)
            {
                return Ok(userModel);
            }
            else
            {
                return BadRequest(userModel);
            }
        }

        [HttpPost]
        public ObjectResult LoginUser(UserDataModel userDataModel)
        {
            userDataModel.model = this.userService.LoginUser(userDataModel.model.UserEmail, userDataModel.model.UserPass);
            if (userDataModel.model.UserId > 0)
            {
                return Ok(userDataModel);
            }
            else
            {
                return BadRequest(userDataModel);
            }
        }

        [HttpPost]
        public ObjectResult SearchUsers(UserDataModel userDataModel)
        {
            this.userService.Search(userDataModel);
            if(userDataModel.searchResults != null)
            {
                return Ok(userDataModel);
            }
            else
            {
                return BadRequest(userDataModel);
            }
        }
    }
}