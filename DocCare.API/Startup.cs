using DocCare.Data.Classes;
using DocCare.Data.DAL;
using DocCare.Data.Interfaces;
using DocCare.Services.ServicesClasses;
using DocCare.Services.ServicesInterfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AutoMapper;
using DocCare.Services.AutoMapper;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System;
using DocCare.Services.ServicesAbstracts;

namespace DocCare.API
{
    public class Startup
    {
        //readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options => 
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddDbContext<DocCareContext>(
                option => option.UseSqlServer(Configuration.GetConnectionString("DocCareConnection")));

            //to inject autoMapper
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapping());
            });
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            //to authentication
            var SecretKey = Encoding.ASCII.GetBytes("DocCareKey-2374-OFFKDI940NG7:56753253-tyuw-5769-0921-kfirox29zoxv");
            services.AddAuthentication(auth =>
            {
                auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(token =>
            {
                token.RequireHttpsMetadata = false;
                token.SaveToken = true;
                token.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(SecretKey),
                    ValidateIssuer = true,
                    ValidIssuer = "http://localhost:58985/",
                    ValidateAudience = true,
                    ValidAudience = "http://localhost:58985/",
                    RequireExpirationTime = true,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };
            });

            // to inject services
            services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));
            services.AddScoped(typeof(IBookingService), typeof(BookingService));
            services.AddScoped(typeof(IDayService), typeof(DayService));
            services.AddScoped(typeof(ISpecialistService), typeof(SpecialistService));
            services.AddScoped(typeof(IHospitalService), typeof(HospitalService));
            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddScoped(typeof(IPaymentMethodService), typeof(PaymentMethodService));
            services.AddScoped(typeof(BookingAlgorithmAbstract), typeof(BookingAlgorithmService));
            //services.AddScoped(typeof(IServiceUnitOfWork), typeof(ServiceUnitOfWork));

            // to inject Repositories
            services.AddScoped(typeof(IDayRepository), typeof(DayRepository));
            services.AddScoped(typeof(IHospitalRepository), typeof(HospitalRepository));
            services.AddScoped(typeof(ISlotRepository), typeof(SlotRepository));
            services.AddScoped(typeof(ISpecialistRepository), typeof(SpecialistRepository));
            services.AddScoped(typeof(IUserRepository), typeof(UserRepository));
            services.AddScoped(typeof(IUserTypeRepository), typeof(UserTypeRepository));
            services.AddScoped(typeof(IAppointmentRepository), typeof(AppointmentRepository));
            services.AddScoped(typeof(IAwardRepository), typeof(AwardRepository));
            services.AddScoped(typeof(IDocServicePriceRepository), typeof(DocServicePriceRepository));
            services.AddScoped(typeof(IDoctorSlotRepository), typeof(DoctorSlotRepository));
            services.AddScoped(typeof(IEducationRepository), typeof(EducationRepository));
            services.AddScoped(typeof(IExperienceRepository), typeof(ExperienceRepository));
            services.AddScoped(typeof(IFavoriteRepository), typeof(FavoriteRepository));
            services.AddScoped(typeof(IMembershipRepository), typeof(MembershipRepository));
            services.AddScoped(typeof(IRegistationRepository), typeof(RegistrationRepository));
            services.AddScoped(typeof(IServiceRepository), typeof(ServiceRepository));
            services.AddScoped(typeof(ISocialMediaRepository), typeof(SocialMediaRepository));

            services.AddScoped(typeof(DbContext), typeof(DocCareContext));

            // to enable CORS
            services.AddCors(c =>
            {
                c.AddPolicy("AllowAll", options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("AllowAll");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllerRoute(
            //        name: "default",
            //        pattern: "{controller=User}/{action=GetInitObject}/{id?}"
            //        );
            //});

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
