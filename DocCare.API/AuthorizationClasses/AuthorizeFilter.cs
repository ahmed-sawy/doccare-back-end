﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DocCare.API.AuthorizationClasses
{
    public class AuthorizeFilter : IAuthorizationFilter
    {
        readonly string[] _claims;
        public AuthorizeFilter(params string[] _claims)
        {
            this._claims = _claims;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var IsAuthenticated = context.HttpContext.User.Identity.IsAuthenticated;
            if (IsAuthenticated)
            {
                bool flagClaim = false;
                //هنا انت ممكن تعمل كل الشغل الي انت عاوزه
                foreach(var item in this._claims)
                {
                    if(context.HttpContext.User.HasClaim("USER_TYPE", item))
                    {
                        flagClaim = true;
                    }
                }
                if (!flagClaim)
                {
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                }
            }
            else
            {
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
            }
        }
    }
}
