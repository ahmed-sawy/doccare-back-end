﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.BaseClasses
{
    public class BaseModel<Model, SearchCrit> where SearchCrit : class
    {
        public enum enStatus { All = -1, NotActive = 0, Active = 1 }
        public enum enOpResultCode { Success = 1, Error = 2 }
        public DateTime serverDate
        {
            get { return DateTime.Now; }
        }
        public PagerSettings pagerSettings { get; set; }
        public SearchCrit searchCrit { get; set; }
        public List<Model> searchResults { get; set; }
        public Model model { get; set; }
        public enOpResultCode opResultCode { get; set; }
        public string opResultMsg { get; set; }
    }
}
