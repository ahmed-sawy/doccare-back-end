﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.BaseClasses
{
    public class PagerSettings
    {
        public PagerSettings() 
        {
            this.SelectedRecsPerPage = 10;
        }
        public int PagesCount { get; set; }
        public int TotalRecords { get; set; }
        public int CurrentPage { get; set; }
        public int SelectedRecsPerPage { get; set; }
        public List<int> Pages { get; set; }
        public List<RecordsPerPage> RecordsPerPages { get; set; }

        public void FillPages()
        {
            this.Pages = new List<int>();
            for(int i = 0; i < this.PagesCount; i++)
            {
                this.Pages.Add(i + 1);
            }
        }
    }
}
