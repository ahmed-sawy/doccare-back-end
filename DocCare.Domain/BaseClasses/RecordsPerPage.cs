﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.BaseClasses
{
    public class RecordsPerPage
    {
        public RecordsPerPage()
        {
            this.RecordsPerPages = this.GetRecordsPerPages();
        }
        public int LabelID { get; set; }
        public string LabelName { get; set; }
        public List<RecordsPerPage> RecordsPerPages { get; set; }

        public List<RecordsPerPage> GetRecordsPerPages()
        {
            return new List<RecordsPerPage>
            {
                new RecordsPerPage
                {
                    LabelID = 10,
                    LabelName = "10 Records"
                },
                new RecordsPerPage
                {
                    LabelID = 25,
                    LabelName = "25 Records"
                },
                new RecordsPerPage
                {
                    LabelID = 50,
                    LabelName = "50 Records"
                },
                new RecordsPerPage
                {
                    LabelID = 100,
                    LabelName = "100 Records"
                }
            };
        }
    }
}
