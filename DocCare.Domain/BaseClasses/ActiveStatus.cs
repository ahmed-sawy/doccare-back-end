﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.BaseClasses
{
    public class ActiveStatus
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }

        public static List<ActiveStatus> GetStatuses()
        {
            return new List<ActiveStatus>
            {
                new ActiveStatus { StatusId = -1, StatusName = "All"},
                new ActiveStatus { StatusId = 0, StatusName = "Not Active"},
                new ActiveStatus { StatusId = 1, StatusName = "Active"}
            };
        }
    }
}
