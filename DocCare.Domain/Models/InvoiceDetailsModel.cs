﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class InvoiceDetailsModel
    {
        public long InvServiceId { get; set; }
        public decimal InvServicePrice { get; set; }
        public decimal InvServiceDiscount { get; set; }
        public decimal InvServiceTotalPrice { get; set; }
        public int InvServiceQuntity { get; set; }
        public int ServiceId { get; set; }
        public long InvoiceId { get; set; }
    }
}
