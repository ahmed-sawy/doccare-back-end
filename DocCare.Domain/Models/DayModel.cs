﻿using DocCare.Data.Entities;
using DocCare.Domain.BaseClasses;
using DocCare.Domain.SearchModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class DayModel
    {
        public int DayId { get; set; }
        public string DayName { get; set; }
        public DateTime DayDate { get; set; }
        public string DayDesc { get; set; }
        public bool DayIsActive { get; set; }
    }
}
