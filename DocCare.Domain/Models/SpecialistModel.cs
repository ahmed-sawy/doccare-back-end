﻿using DocCare.Data.Entities;
using DocCare.Domain.BaseClasses;
using DocCare.Domain.SearchModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class SpecialistModel
    {
        public int SpecialistId { get; set; }
        public string SpecialistName { get; set; }
        public string SpecialistDesc { get; set; }
        public bool SpecialistIsActive { get; set; }
        public int HospitalId { get; set; }
    }
}
