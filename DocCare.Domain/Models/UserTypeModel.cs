﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class UserTypeModel
    {
        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }
        public string UserTypeDesc { get; set; }
        public bool UserTypeIsActive { get; set; }
        public int HospitalId { get; set; }
    }
}
