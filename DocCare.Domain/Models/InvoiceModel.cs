﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class InvoiceModel
    {
        public long InvoiceId { get; set; }
        public string InvoiceNo { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal InvoiceDiscount { get; set; }
        public decimal InvoiceTotalAmount { get; set; }
        public DateTime InvoiceIssueDate { get; set; }
        public DateTime InvoicePaidDate { get; set; }
        public string InvoiceInfo { get; set; }
        public long DoctorId { get; set; }
        public long PatientId { get; set; }
        public int StatusId { get; set; }
        public int PaymentId { get; set; }
        public long AppointmentId { get; set; }


        public AppointmentModel Appointment { get; set; }
    }
}
