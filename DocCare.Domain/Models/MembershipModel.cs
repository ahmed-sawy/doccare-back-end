﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class MembershipModel
    {
        public int MembershipId { get; set; }
        public string Memberships { get; set; }
        public long DoctorId { get; set; }

        public UserModel Doctor { get; set; }
    }
}
