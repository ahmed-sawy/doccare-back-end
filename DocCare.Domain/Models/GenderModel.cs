﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class GenderModel
    {
        public int GenderId { get; set; }
        public string GenderName { get; set; }
        public bool GenderIsActive { get; set; }

    }
}
