﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class AppointmentModel
    {
        public long AppointmentId { get; set; }
        public DateTime AppointmentBookDate { get; set; }
        public DateTime AppointmentDate { get; set; }
        public TimeSpan AppointmentFrom { get; set; }
        public TimeSpan AppointmentTo { get; set; }
        public decimal AppointmentAmount { get; set; }
        public long PatientId { get; set; }
        public long DoctorId { get; set; }

        public UserModel Patient { get; set; }
        public UserModel Doctor { get; set; }
        public List<InvoiceModel> Invoices { get; set; }
    }
}
