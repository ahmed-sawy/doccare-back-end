﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class PaymentMethodModel
    {
        public int PaymentId { get; set; }
        public string PaymentName { get; set; }
        public bool PaymentIsActive { get; set; }
    }
}
