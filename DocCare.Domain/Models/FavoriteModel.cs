﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class FavoriteModel
    {
        public long PatientId { get; set; }
        public long DoctorId { get; set; }

        public UserModel Patient { get; set; }
        public UserModel Doctor { get; set; }
    }
}
