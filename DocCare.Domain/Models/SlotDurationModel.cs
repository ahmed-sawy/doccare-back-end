﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class SlotDurationModel
    {
        public int SlotDurationId { get; set; }
        public string SlotDurationName { get; set; }
        public string SlotDurationDesc { get; set; }
        public decimal SlotDurationValue { get; set; }
        public bool SlotDurationIsActive { get; set; }
        public int HospitalId { get; set; }
    }
}
