﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class DocServicePriceModel
    {
        public long DoctorId { get; set; }
        public int ServiceId { get; set; }
        public decimal ServicePrice { get; set; }

        public UserModel Doctor { get; set; }
        public ServiceModel Service { get; set; }
    }
}
