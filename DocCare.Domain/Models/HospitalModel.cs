﻿using DocCare.Data.Entities;
using DocCare.Domain.BaseClasses;
using DocCare.Domain.SearchModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class HospitalModel
    {
        public int HospitalId { get; set; }
        public string HospitalName { get; set; }
        public string HospitalAddress { get; set; }
        public string HospitalPhone1 { get; set; }
        public string HospitalPhone2 { get; set; }
        public string HospitalEmail { get; set; }
        public bool HospitalIsActive { get; set; }
    }
}
