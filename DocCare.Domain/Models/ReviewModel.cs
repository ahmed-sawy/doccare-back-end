﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class ReviewModel
    {
        public int ReviewId { get; set; }
        public string ReviewTitle { get; set; }
        public string ReviewDetails { get; set; }
        public int ReviewStars { get; set; }
        public DateTime ReviewDate { get; set; }
        public bool ReviewIsActive { get; set; }
        public int ReviewParent { get; set; } //if = 0 it means that is main review if greater than 0 it means sub review (Replay)
        public long PatientId { get; set; }
        public long DoctorId { get; set; }
    }
}
