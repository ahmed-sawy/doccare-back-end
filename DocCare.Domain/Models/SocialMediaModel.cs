﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class SocialMediaModel
    {
        public long SocialMediaId { get; set; }
        public string FaceBook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string Pinterest { get; set; }
        public string LinkedIn { get; set; }
        public string Youtube { get; set; }
        public long DoctorId { get; set; }

        public UserModel Doctor { get; set; }
    }
}
