﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class EducationModel
    {
        public int EducationId { get; set; }
        public string EducationDegree { get; set; }
        public string EducationCollege { get; set; }
        public DateTime EducationCompletionYear { get; set; }
        public long DoctorId { get; set; }

        public UserModel Doctor { get; set; }
    }
}
