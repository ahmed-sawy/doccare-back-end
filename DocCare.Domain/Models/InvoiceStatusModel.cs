﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class InvoiceStatusModel
    {
        public int InvoiceStatusId { get; set; }
        public string InvoiceStatusName { get; set; }
        public string InvoiceStatusDesc { get; set; }
        public bool InvoiceStatusIsActive { get; set; }
        public int HospitalId { get; set; }
    }
}
