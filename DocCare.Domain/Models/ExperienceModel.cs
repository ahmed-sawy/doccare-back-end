﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class ExperienceModel
    {
        public int ExperienceId { get; set; }
        public string ExperienceName { get; set; }
        public string ExperienceDesc { get; set; }
        public DateTime ExperienceFrom { get; set; }
        public DateTime ExperienceTo { get; set; }
        public long DoctorId { get; set; }

        public UserModel Doctor { get; set; }
    }
}
