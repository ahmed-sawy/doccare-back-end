﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class ServiceModel
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string ServiceDesc { get; set; }
        public bool ServiceIsActive { get; set; }
        public int SpecialistId { get; set; }

        public SpecialistModel Specialist { get; set; }
    }
}
