﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class UserModel
    {
        public long UserId { get; set; }
        public string UserCode { get; set; }
        public string UserTitle { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmail { get; set; }
        public string UserPhone { get; set; }
        public string UserPass { get; set; }
        public bool UserIsActive { get; set; }
        public decimal UserServicePrice { get; set; }
        public int UserTypeId { get; set; }
        public int HospitalId { get; set; }
        public int GenderId { get; set; }
        public int BloodGroupId { get; set; }
        public string UserToken { get; set; }

        public List<AppointmentModel> Appointments { get; set; }
        public List<ScheduleTimingModel> ScheduleTimings { get; set; }
        public List<AwardModel> Awards { get; set; }
        public List<DocServicePriceModel> DocServicePrices { get; set; }
        //public List<DoctorSlotModel> DoctorSlots { get; set; }
        public List<EducationModel> Educations { get; set; }
        public List<ExperienceModel> Experiences { get; set; }
        public List<FavoriteModel> Favorites { get; set; }
        public HospitalModel Hospital { get; set; }
        public List<MembershipModel> Memberships { get; set; }
        public List<RegistrationModel> Registrations { get; set; }
        public SocialMediaModel SocialMedia { get; set; }
        public SpecialistModel Specialist { get; set; }
        public GenderModel Gender { get; set; }
    }
}
