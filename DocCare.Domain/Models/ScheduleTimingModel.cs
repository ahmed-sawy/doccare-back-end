﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class ScheduleTimingModel
    {
        public int ScheduleId { get; set; }
        public TimeSpan ScheduleFrom { get; set; }
        public TimeSpan ScheduleTo { get; set; }
        public int AppointmentsNo { get; set; }
        public long DoctorId { get; set; }
        public int DayId { get; set; }

        public DayModel Day { get; set; }
        public List<SlotModel> Slots { get; set; }
    }
}
