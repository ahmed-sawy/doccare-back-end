﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class AwardModel
    {
        public int AwardId { get; set; }
        public string Awards { get; set; }
        public DateTime AwardYear { get; set; }
        public long DoctorId { get; set; }

        public UserModel Doctor { get; set; }
    }
}
