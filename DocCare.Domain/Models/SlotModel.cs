﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class SlotModel
    {
        public long SlotId { get; set; }
        public TimeSpan SlotTimeFrom { get; set; }
        public TimeSpan SlotTimeTo { get; set; }
        public bool SlotIsBooked { get; set; }
        public int ScheduleId { get; set; }
    }
}
