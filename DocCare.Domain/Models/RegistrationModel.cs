﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.Models
{
    public class RegistrationModel
    {
        public int RegistrationId { get; set; }
        public string Registrations { get; set; }
        public DateTime RegistrationYear { get; set; }
        public long DoctorId { get; set; }

        public UserModel Doctor { get; set; }
    }
}
