﻿using DocCare.Data.Entities;
using DocCare.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.SearchModels
{
    public class SpecialistsSearch
    {
        public SpecialistsSearch()
        {
            this.SpecialistName = "";
            this.Hospital = new Hospital();
            this.IsActive = -1;
            this.SearchResults = new List<SpecialistModel>();
        }
        public string SpecialistName { get; set; }
        public Hospital Hospital { get; set; }
        public int IsActive { get; set; }
        public List<SpecialistModel> SearchResults { get; set; }
    }
}
