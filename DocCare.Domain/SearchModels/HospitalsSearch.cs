﻿using DocCare.Domain.Models;
using System.Collections.Generic;

namespace DocCare.Domain.SearchModels
{
    public class HospitalsSearch
    {
        public HospitalsSearch()
        {
            this.SearchResults = new List<HospitalModel>();
            this.HospitalName = "";
            this.HospitalPhone = "";
            this.HospitalEmail = "";
            this.IsActive = -1;
        }
        public List<HospitalModel> SearchResults { get; set; }
        public string HospitalName { get; set; }
        public string HospitalPhone { get; set; }
        public string HospitalEmail { get; set; }
        public int IsActive { get; set; }
    }
}
