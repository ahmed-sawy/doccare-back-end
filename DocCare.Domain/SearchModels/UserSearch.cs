﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.SearchModels
{
    public class UserSearch
    {
        public string UserName { get; set; }
        public string Specialist { get; set; }
        public string Hospital { get; set; }
        public int UserType { get; set; }
    }
}
