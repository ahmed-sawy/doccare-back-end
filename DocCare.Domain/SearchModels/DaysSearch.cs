﻿using DocCare.Domain.BaseClasses;
using DocCare.Domain.Models;
using System.Collections.Generic;

namespace DocCare.Domain.SearchModels
{
    public class DaysSearch
    {
        public DaysSearch()
        {
            this.DayName = "";
            this.Status = -1;
            this.ActiveStatuses = ActiveStatus.GetStatuses();
            //this.SearchResults = new List<DayModel>();
        }
        public string DayName { get; set; }
        public int Status { get; set; }
        public List<ActiveStatus> ActiveStatuses { get; set; }
        //public List<DayModel> SearchResults { get; set; }
    }
}
