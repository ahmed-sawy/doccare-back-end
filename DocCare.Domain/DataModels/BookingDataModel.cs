﻿using DocCare.Domain.Models;
using System.Collections.Generic;

namespace DocCare.Domain.DataModels
{
    public class BookingDataModel
    {
        public UserModel Doctor { get; set; }
        public UserModel Patient { get; set; }
        public SlotModel SelectedSlot { get; set; }
        public ScheduleTimingModel SelectedScheduleTiming { get; set; }
        public PaymentMethodModel SelectedPaymentMethod { get; set; }

        public List<PaymentMethodModel> PaymentMethods { get; set; }
        public List<ScheduleTimingModel> ScheduleTimings { get; set; }

        public int AvailableDaysNo { get; set; }

        public BookingDataModel(int availableDaysNo)
        {
            this.AvailableDaysNo = availableDaysNo;
        }
    }
}
