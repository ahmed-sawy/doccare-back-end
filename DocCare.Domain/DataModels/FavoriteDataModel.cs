﻿using DocCare.Domain.BaseClasses;
using DocCare.Domain.Models;
using DocCare.Domain.SearchModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Domain.DataModels
{
    public class FavoriteDataModel : BaseModel<FavoriteModel, FavoriteSearch>
    {
    }
}
