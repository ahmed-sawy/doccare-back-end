﻿using DocCare.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Data.DAL
{
    public class DocCareContext : DbContext
    {
        public DocCareContext(DbContextOptions<DocCareContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DocServicePrice>().HasKey(d => new
            {
                d.ServiceId,
                d.DoctorId
            });

            modelBuilder.Entity<Favorite>().HasKey(f => new
            {
                f.DoctorId,
                f.PatientId
            });

            modelBuilder.Entity<HospitalPaymentMethod>().HasKey(h => new
            {
                h.HospitalId,
                h.PaymentId
            });
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<ScheduleTiming> ScheduleTimings { get; set; }
        public DbSet<SocialMedia> SocialMedias { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Membership> Memberships { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<DocServicePrice> DocServicePrices { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<Favorite> Favorites { get; set; }
        public DbSet<Experience> Experiences { get; set; }
        public DbSet<Award> Awards { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Hospital> Hospitals { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<InvoiceStatus> InvoiceStatuses { get; set; }
        public DbSet<PatientType> PatientTypes { get; set; }
        public DbSet<Specialist> Specialists { get; set; }
        public DbSet<BloodGroup> BloodGroups { get; set; }
        public DbSet<SlotDuration> SlotDurations { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<HospitalPaymentMethod> HospitalPaymentMethods { get; set; }
        public DbSet<Day> Days { get; set; }
        public DbSet<Slot> Slots { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<InvoiceDetails> InvoiceDetails { get; set; }
        //public DbSet<State> States { get; set; }
        //public DbSet<City> Cities { get; set; }
        //public DbSet<Country> Countries { get; set; }

    }
}
