﻿using DocCare.Data.DAL;
using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DocCare.Data.Classes
{
    public class SlotRepository : GenericRepository<Slot>, ISlotRepository
    {
        public SlotRepository(DbContext _context) 
            : base(_context)
        {
        }

        public DocCareContext _docCareContext
        {
            get { return _context as DocCareContext; }
        }
    }
}
