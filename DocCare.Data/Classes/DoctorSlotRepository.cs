﻿using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Data.Classes
{
    public class DoctorSlotRepository : GenericRepository<DoctorSlot>, IDoctorSlotRepository
    {
        public DoctorSlotRepository(DbContext _context) : base(_context)
        {
        }
    }
}
