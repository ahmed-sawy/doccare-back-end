﻿using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Data.Classes
{
    public class EducationRepository : GenericRepository<Education>, IEducationRepository
    {
        public EducationRepository(DbContext _context) : base(_context)
        {
        }
    }
}
