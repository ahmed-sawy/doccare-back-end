﻿using DocCare.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DocCare.Data.Classes
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext _context;

        public GenericRepository(DbContext _context)
        {
            this._context = _context;
        }
        public void Add(TEntity oEntity)
        {
            this._context.Set<TEntity>().Add(oEntity);
        }

        public void AddRange(List<TEntity> lEntities)
        {
            this._context.Set<TEntity>().AddRange(lEntities);
        }

        public List<TEntity> GetAll()
        {
            return this._context.Set<TEntity>().ToList();
        }

        public TEntity GetByID(long ID)
        {
            return this._context.Set<TEntity>().Find(ID);
        }

        public void Remove(TEntity oEntity)
        {
            this._context.Set<TEntity>().Remove(oEntity);
        }

        public void RemoveRange(List<TEntity> lEntities)
        {
            this._context.Set<TEntity>().RemoveRange(lEntities);
        }

        public List<TEntity> Search(Expression<Func<TEntity, bool>> predicate)
        {
            return this._context.Set<TEntity>().Where(predicate).ToList();
        }

        public void Update(TEntity oEntity)
        {
            this._context.Entry(oEntity).State = EntityState.Modified;
        }
    }
}
