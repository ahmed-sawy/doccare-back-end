﻿using DocCare.Data.DAL;
using DocCare.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Data.Classes
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;
        //private readonly IDayRepository dayRepository;

        public UnitOfWork(DbContext _context, IDayRepository dayRepository, IHospitalRepository hospitalRepository,
            ISlotRepository slotRepository, ISpecialistRepository specialistRepository,
            IUserRepository userRepository, IUserTypeRepository userTypeRepository, IAppointmentRepository appointmentRepository,
            IAwardRepository awardRepository, IDocServicePriceRepository docServicePriceRepository, IDoctorSlotRepository doctorSlotRepository,
            IEducationRepository educationRepository, IExperienceRepository experienceRepository, IFavoriteRepository favoriteRepository,
            IMembershipRepository membershipRepository, IRegistationRepository registationRepository, IServiceRepository serviceRepository,
            ISocialMediaRepository socialMediaRepository)
        {
            this._context = _context;

            this.Days = dayRepository;
            this.Hospitals = hospitalRepository;
            this.Slots = slotRepository;
            this.Specialists = specialistRepository;
            this.Users = userRepository;
            this.UserTypes = userTypeRepository;
            this.Appointments = appointmentRepository;
            this.Awards = awardRepository;
            this.DocServicePrices = docServicePriceRepository;
            this.DoctorSlots = doctorSlotRepository;
            this.Educations = educationRepository;
            this.Experiences = experienceRepository;
            this.Favorites = favoriteRepository;
            this.Memberships = membershipRepository;
            this.Registations = registationRepository;
            this.Services = serviceRepository;
            this.SocialMedias = socialMediaRepository;
        }

        public IDayRepository Days { get; private set; }
        public IHospitalRepository Hospitals { get; private set; }
        public ISlotRepository Slots { get; private set; }
        public ISpecialistRepository Specialists { get; private set; }
        public IUserRepository Users { get; private set; }
        public IUserTypeRepository UserTypes { get; private set; }
        public IAppointmentRepository Appointments { get; private set; }
        public IAwardRepository Awards { get; private set; }
        public IDocServicePriceRepository DocServicePrices { get; private set; }
        public IDoctorSlotRepository DoctorSlots { get; private set; }
        public IEducationRepository Educations { get; private set; }
        public IExperienceRepository Experiences { get; private set; }
        public IFavoriteRepository Favorites { get; private set; }
        public IMembershipRepository Memberships { get; private set; }
        public IRegistationRepository Registations { get; private set; }
        public IServiceRepository Services { get; private set; }
        public ISocialMediaRepository SocialMedias { get; private set; }


        public int Complete()
        {
            return this._context.SaveChanges();
        }

        public void Dispose()
        {
            this._context.Dispose();
        }
    }
}
