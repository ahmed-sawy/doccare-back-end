﻿using DocCare.Data.DAL;
using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCare.Data.Classes
{
    public class HospitalRepository : GenericRepository<Hospital>, IHospitalRepository
    {
        public HospitalRepository(DbContext _context) 
            : base(_context)
        {
        }

        public DocCareContext _docCareContext
        {
            get { return _context as DocCareContext; }
        }

        public Hospital GetHospitalDetails(int hospitalId)
        {
            try
            {
                Hospital hospital = this._context.Set<Hospital>().Where(h => h.HospitalId == hospitalId)
                    .Include(h => h.HospitalPaymentMethods)
                        .ThenInclude(hp => hp.PaymentMethod)
                    .FirstOrDefault();
                if (hospital != null && hospital.HospitalId > 0)
                    return hospital;
                else
                    return new Hospital();
            }
            catch
            {
                return null;
            }
        }
    }
}
