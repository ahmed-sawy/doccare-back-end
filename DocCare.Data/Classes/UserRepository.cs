﻿using DocCare.Data.DAL;
using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DocCare.Data.Classes
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private enum userTypes { Admin = 1, Doctor = 2, Patient = 3 };

        public UserRepository(DbContext _context)
            : base(_context)
        {
        }

        public User LoginUser(string UserEmail, string UserPass)
        {
            return this._context.Set<User>().FirstOrDefault(u => u.UserEmail == UserEmail && u.UserPass == UserPass);
        }

        public User GetByIdWithRelatedData(long ID)
        {
            try
            {
                var user = this._context.Set<User>().Find(ID);

                if (user != null)
                {
                    if (user.UserTypeId == (int)userTypes.Admin)
                    {

                    }
                    else if (user.UserTypeId == (int)userTypes.Doctor)
                    {
                        this._context.Entry(user)
                            .Collection(u => u.Awards).Load();
                        this._context.Entry(user)
                            .Collection(u => u.Educations).Load();
                        this._context.Entry(user)
                            .Collection(u => u.Experiences).Load();
                        this._context.Entry(user)
                            .Collection(u => u.Memberships).Load();
                        this._context.Entry(user)
                            .Collection(u => u.Registrations).Load();
                        this._context.Entry(user)
                            .Collection(u => u.DoctorAppointments).Load();
                        this._context.Entry(user)
                            .Collection(u => u.DoctorInvoices).Load();
                        this._context.Entry(user)
                            .Collection(u => u.DoctorReviews).Load();
                        this._context.Entry(user)
                            .Property(u => u.SocialMedia);
                    }
                    else if (user.UserTypeId == (int)userTypes.Patient)
                    {
                        this._context.Entry(user)
                            .Collection(u => u.PatientAppointments).Load();
                        this._context.Entry(user)
                            .Collection(u => u.PatientFavorites).Load();
                        this._context.Entry(user)
                            .Collection(u => u.PatientInvoices).Load();
                    }
                    else
                    {

                    }

                }

                return user;
            }
            catch (Exception ex)
            {
                string exep = ex.Message;
                return new User();
            }
        }

        public List<User> SearchUsersWithRelatedData(Expression<Func<User, bool>> predicate)
        {
            try
            {
                //List<User> users = this._context.Set<User>().Where(predicate).ToList();

                List<User> users = this._context.Set<User>().Where(predicate)
                    .Include(u => u.Educations)
                    .Include(u => u.Experiences)
                    .Include(u => u.DoctorReviews)
                    .Include(u => u.DocServicePrices).ThenInclude(srv => srv.Service).ThenInclude(spc => spc.Specialist)
                    .Include(u => u.Hospital)
                    .Include(u => u.Gender).ToList();

                if (users != null && users.Count > 0)
                {
                    foreach (User user in users)
                    {
                        user.UserPass = "";
                    }

                }

                return users;
            }
            catch (Exception ex)
            {
                string exep = ex.Message;
                return null;
            }
        }

        public User GetByIdWithScheduleTiming(long ID)
        {
            try
            {
                User user = this._context.Set<User>().Where(u => u.UserId == ID)
                    .Include(u => u.ScheduleTimings)
                        .ThenInclude(schd => schd.Day)
                    .Include(u => u.ScheduleTimings)
                        .ThenInclude(schd => schd.Slots)
                    .FirstOrDefault();

                if(user != null && user.UserId > 0 && user.UserTypeId == (int)userTypes.Doctor
                    && user.ScheduleTimings != null && user.ScheduleTimings.Count > 0)
                {
                    return user;
                }

                return new User();
            }
            catch (Exception ex)
            {
                string exep = ex.Message;
                return new User();
            }
        }

        //public List<User> GetByUsernameStored(string username)
        //{
        //    //var res = this._context.Database.ExecuteSqlCommand("FirstStored @p0", parameters: new[] { username });
        //    //var res = this._context.Set<User>().FromSqlRaw("FirstStored {0}", username).ToList();
        //    return null;
        //}

        public DocCareContext _docCareContext
        {
            get { return _context as DocCareContext; }
        }

    }
}
