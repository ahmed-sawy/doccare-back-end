﻿using DocCare.Data.Entities;
using DocCare.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocCare.Data.Classes
{
    public class SocialMediaRepository : GenericRepository<SocialMedia>, ISocialMediaRepository
    {
        public SocialMediaRepository(DbContext _context) 
            : base(_context)
        {
        }

        public SocialMedia GetSocialMediaByDoctorId(long doctorId)
        {
            return this._context.Set<SocialMedia>().FirstOrDefault(s => s.DoctorId == doctorId);
        }
    }
}
