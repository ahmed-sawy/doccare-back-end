﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Experience
    {
        [Key]
        public int ExperienceId { get; set; }
        [Required]
        [MaxLength(500)]
        public string ExperienceName { get; set; }
        [Required]
        [MaxLength(2000)]
        public string ExperienceDesc { get; set; }
        [Required]
        public DateTime ExperienceFrom { get; set; }
        [Required]
        public DateTime ExperienceTo { get; set; }
        [Required]
        public bool ExperienceIsPresent { get; set; }
        [Required]
        public long DoctorId { get; set; }


        [ForeignKey("DoctorId")]
        [InverseProperty("Experiences")]
        public virtual User Doctor { get; set; }
    }
}
