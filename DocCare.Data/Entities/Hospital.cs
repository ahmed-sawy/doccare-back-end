﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Hospital
    {
        [Key]
        public int HospitalId { get; set; }
        [Required]
        [MaxLength(200)]
        public string HospitalName { get; set; }
        [Required]
        [MaxLength(500)]
        public string HospitalAddress { get; set; }
        [Required]
        [Phone]
        public string HospitalPhone1 { get; set; }
        [Phone]
        public string HospitalPhone2 { get; set; }
        public string HospitalEmail { get; set; }
        public bool HospitalIsActive { get; set; }



        public ICollection<User> Users { get; set; }
        public ICollection<UserType> UserTypes { get; set; }
        public ICollection<InvoiceStatus> InvoiceStatuses { get; set; }
        public ICollection<PatientType> PatientTypes { get; set; }
        public ICollection<Specialist> Specialists { get; set; }
        public ICollection<BloodGroup> BloodGroups { get; set; }
        public ICollection<SlotDuration> SlotDurations { get; set; }
        public ICollection<HospitalPaymentMethod> HospitalPaymentMethods { get; set; }

        //public IEnumerable<Doc> Slots { get; set; }
        //public ICollection<Day> Days { get; set; }
    }
}
