﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class BloodGroup
    {
        [Key]
        public int GroupId { get; set; }
        [Required]
        [MaxLength(50)]
        public string GroupName { get; set; }
        [MaxLength(2000)]
        public string GroupDesc { get; set; }
        [Required]
        public bool GroupIsActive { get; set; }
        [Required]
        public int HospitalId { get; set; }


        [ForeignKey("HospitalId")]
        [InverseProperty("BloodGroups")]
        public virtual Hospital Hospital { get; set; }


        public ICollection<User> Users { get; set; }
    }
}
