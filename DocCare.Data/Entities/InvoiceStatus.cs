﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class InvoiceStatus
    {
        [Key]
        public int InvoiceStatusId { get; set; }
        [Required]
        [MaxLength(200)]
        public string InvoiceStatusName { get; set; }
        [MaxLength(2000)]
        public string InvoiceStatusDesc { get; set; }
        [Required]
        public bool InvoiceStatusIsActive { get; set; }
        [Required]
        public int HospitalId { get; set; }


        [ForeignKey("HospitalId")]
        [InverseProperty("InvoiceStatuses")]
        public virtual Hospital Hospital { get; set; }


        public ICollection<Invoice> Invoices { get; set; }
    }
}
