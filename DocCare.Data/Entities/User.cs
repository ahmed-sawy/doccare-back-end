﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class User
    {
        [Key]
        public long UserId { get; set; }
        [Required]
        [MaxLength(50)]
        public string UserCode { get; set; }
        [MaxLength(200)]
        public string UserTitle { get; set; }
        [Required]
        [MaxLength(50)]
        public string UserFirstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string UserLastName { get; set; }
        [Required]
        public DateTime UserDateofBirth { get; set; }
        [EmailAddress]
        public string UserEmail { get; set; }
        [Required]
        [Phone]
        public string UserPhone { get; set; }
        [Required]
        public string UserPass { get; set; }
        [Required]
        public bool UserIsActive { get; set; }
        public decimal UserServicePrice { get; set; } //If user is a doctor
        [Required]
        public int HospitalId { get; set; }
        [Required]
        public int UserTypeId { get; set; }
        [Required]
        public int GenderId { get; set; }
        [Required]
        public int BloodGroupId { get; set; }


        public virtual SocialMedia SocialMedia { get; set; }
        [ForeignKey("HospitalId")]
        [InverseProperty("Users")]
        public virtual Hospital Hospital { get; set; }
        [ForeignKey("UserTypeId")]
        [InverseProperty("Users")]
        public virtual UserType UserType { get; set; }
        [ForeignKey("GenderId")]
        [InverseProperty("Users")]
        public virtual Gender Gender { get; set; }
        [ForeignKey("BloodGroupId")]
        [InverseProperty("Users")]
        public virtual BloodGroup BloodGroup { get; set; }

        public ICollection<ScheduleTiming> ScheduleTimings { get; set; }
        public ICollection<Registration> Registrations { get; set; }
        public ICollection<Review> PatientReviews { get; set; }
        public ICollection<Review> DoctorReviews { get; set; }
        public ICollection<Membership> Memberships { get; set; }
        public ICollection<Invoice> DoctorInvoices { get; set; }
        public ICollection<Invoice> PatientInvoices { get; set; }
        public ICollection<DocServicePrice> DocServicePrices { get; set; }
        public ICollection<Education> Educations { get; set; }
        public ICollection<Favorite> PatientFavorites { get; set; }
        public ICollection<Experience> Experiences { get; set; }
        public ICollection<Award> Awards { get; set; }
        public ICollection<Appointment> DoctorAppointments { get; set; }
        public ICollection<Appointment> PatientAppointments { get; set; }

        

        //public ICollection<Membership> Memberships { get; set; }
        //public ICollection<Registration> Registrations { get; set; }
        //public ICollection<Appointment> Appointments { get; set; }
    }
}
