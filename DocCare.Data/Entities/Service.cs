﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Service
    {
        [Key]
        public int ServiceId { get; set; }
        [Required]
        [MaxLength(200)]
        public string ServiceName { get; set; }
        [MaxLength(2000)]
        public string ServiceDesc { get; set; }
        public bool ServiceIsActive { get; set; }
        [Required]
        public int SpecialistId { get; set; }


        [ForeignKey("SpecialistId")]
        [InverseProperty("Services")]
        public virtual Specialist Specialist { get; set; }


        public ICollection<DocServicePrice> DocServicePrices { get; set; }
        public ICollection<InvoiceDetails> InvoiceDetails { get; set; }

    }
}
