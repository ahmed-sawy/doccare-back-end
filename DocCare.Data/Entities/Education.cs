﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Education
    {
        [Key]
        public int EducationId { get; set; }
        [Required]
        [MaxLength(2000)]
        public string EducationDegree { get; set; }
        [Required]
        [MaxLength(500)]
        public string EducationCollege { get; set; }
        [Required]
        public DateTime EducationCompletionYear { get; set; }
        [Required]
        public long DoctorId { get; set; }


        [ForeignKey("DoctorId")]
        [InverseProperty("Educations")]
        public virtual User Doctor { get; set; }
    }
}
