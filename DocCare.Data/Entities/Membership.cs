﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Membership
    {
        [Key]
        public int MembershipId { get; set; }
        [Required]
        [MaxLength(2000)]
        public string Memberships { get; set; }
        [Required]
        public long DoctorId { get; set; }


        [ForeignKey("DoctorId")]
        [InverseProperty("Memberships")]
        public virtual User Doctor { get; set; }
    }
}
