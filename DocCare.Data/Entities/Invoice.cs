﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Invoice
    {
        [Key]
        public long InvoiceId { get; set; }
        [Required]
        [MaxLength(100)]
        public string InvoiceNo { get; set; }
        [Required]
        public decimal InvoiceAmount { get; set; }
        [Required]
        public decimal InvoiceDiscount { get; set; }
        [Required]
        public decimal InvoiceTotalAmount { get; set; }
        [Required]
        public DateTime InvoiceIssueDate { get; set; }
        [Required]
        public DateTime InvoicePaidDate { get; set; }
        [MaxLength(2000)]
        public string InvoiceInfo { get; set; }
        [Required]
        public long DoctorId { get; set; }
        [Required]
        public long PatientId { get; set; }
        [Required]
        public int StatusId { get; set; }
        [Required]
        public int PaymentId { get; set; }
        [Required]
        public long AppointmentId { get; set; }



        [ForeignKey("DoctorId")]
        [InverseProperty("DoctorInvoices")]
        public virtual User DoctorInvoice { get; set; }
        [ForeignKey("PatientId")]
        [InverseProperty("PatientInvoices")]
        public virtual User PatientInvoice { get; set; }
        [ForeignKey("StatusId")]
        [InverseProperty("Invoices")]
        public virtual InvoiceStatus InvoiceStatus { get; set; }
        [ForeignKey("PaymentId")]
        [InverseProperty("Invoices")]
        public virtual PaymentMethod PaymentMethod { get; set; }
        [ForeignKey("AppointmentId")]
        [InverseProperty("Invoices")]
        public virtual Appointment Appointment { get; set; }


        public ICollection<InvoiceDetails> InvoiceDetails { get; set; }
    }
}
