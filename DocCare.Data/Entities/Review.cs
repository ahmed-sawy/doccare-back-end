﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Review
    {
        [Key]
        public int ReviewId { get; set; }
        [Required]
        [MaxLength(200)]
        public string ReviewTitle { get; set; }
        [MaxLength(2000)]
        public string ReviewDetails { get; set; }
        public int ReviewStars { get; set; }
        [Required]
        public DateTime ReviewDate { get; set; }
        [Required]
        public bool ReviewIsActive { get; set; }
        [Required]
        public int ReviewParent { get; set; } //if = 0 it means that is main review if greater than 0 it means sub review (Replay)
        [Required]
        public long PatientId { get; set; }
        [Required]
        public long DoctorId { get; set; }

        [ForeignKey("PatientId")]
        [InverseProperty("PatientReviews")]
        public virtual User Patient { get; set; }
        [ForeignKey("DoctorId")]
        [InverseProperty("DoctorReviews")]
        public virtual User Doctor { get; set; }
    }
}
