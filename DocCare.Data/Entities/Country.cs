﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Country
    {
        [Key]
        public int CountryId { get; set; }
        [Required]
        [MaxLength(100)]
        public string CountryName { get; set; }
        [Required]
        public bool CountryIsActive { get; set; }

        public ICollection<City> Cities { get; set; }
    }
}
