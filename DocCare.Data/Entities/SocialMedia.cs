﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class SocialMedia
    {
        [Key]
        public long SocialMediaId { get; set; }
        public string FaceBook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string Pinterest { get; set; }
        public string LinkedIn { get; set; }
        public string Youtube { get; set; }
        [Required]
        public long DoctorId { get; set; }

        [ForeignKey("DoctorId")]
        [InverseProperty("SocialMedia")]
        public virtual User Doctor { get; set; }
    }
}
