﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class HospitalPaymentMethod
    {
        public int HospitalId { get; set; }
        public int PaymentId { get; set; }



        [ForeignKey("HospitalId")]
        [InverseProperty("HospitalPaymentMethods")]
        public virtual Hospital Hospital { get; set; }

        [ForeignKey("PaymentId")]
        [InverseProperty("HospitalPaymentMethods")]
        public virtual PaymentMethod PaymentMethod { get; set; }
    }
}
