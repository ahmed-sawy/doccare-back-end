﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class UserType
    {
        [Key]
        public int UserTypeId { get; set; }
        [Required]
        [MaxLength(100)]
        public string UserTypeName { get; set; }
        [MaxLength(2000)]
        public string UserTypeDesc { get; set; }
        public bool UserTypeIsActive { get; set; }
        [Required]
        public int HospitalId { get; set; }


        [ForeignKey("HospitalId")]
        [InverseProperty("UserTypes")]
        public virtual Hospital Hospital { get; set; }


        public ICollection<User> Users { get; set; }

    }
}
