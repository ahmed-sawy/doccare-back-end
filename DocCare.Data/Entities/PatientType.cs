﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class PatientType
    {
        [Key]
        public int PatientTypeId { get; set; }
        [Required]
        [MaxLength(200)]
        public string PatientTypeName { get; set; }
        [MaxLength(2000)]
        public string PatientTypeDesc { get; set; }
        [Required]
        public bool PatientTypeIsActive { get; set; }
        [Required]
        public int HospitalId { get; set; }


        [ForeignKey("HospitalId")]
        [InverseProperty("PatientTypes")]
        public virtual Hospital Hospital { get; set; }
    }
}
