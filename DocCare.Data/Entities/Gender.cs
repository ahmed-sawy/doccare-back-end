﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace DocCare.Data.Entities
{
    public class Gender
    {
        [Key]
        public int GenderId { get; set; }
        [Required]
        [MaxLength(50)]
        public string GenderName { get; set; }
        [Required]
        public bool GenderIsActive { get; set; }


        public ICollection<User> Users { get; set; }
    }
}
