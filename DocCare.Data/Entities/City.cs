﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DocCare.Data.Entities
{
    public class City
    {
        [Key]
        public int CityId { get; set; }
        [Required]
        [MaxLength(100)]
        public string CityName { get; set; }
        [Required]
        public bool CityIsActive { get; set; }
        [Required]
        public int CountryId { get; set; }

        public Country Country { get; set; }
        public ICollection<State> States { get; set; }
    }
}
