﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Favorite
    {
        public long PatientId { get; set; }
        public long DoctorId { get; set; }

        [ForeignKey("PatientId")]
        [InverseProperty("PatientFavorites")]
        public virtual User Patient { get; set; }
        [ForeignKey("DoctorId")]
        public virtual User Doctor { get; set; }
    }
}
