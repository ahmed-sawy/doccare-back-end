﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class DocServicePrice
    {
        [Required]
        public long DoctorId { get; set; }
        [Required]
        public int ServiceId { get; set; }
        [Required]
        public decimal ServicePrice { get; set; }

        [ForeignKey("DoctorId")]
        [InverseProperty("DocServicePrices")]
        public virtual User Doctor { get; set; }
        [ForeignKey("ServiceId")]
        [InverseProperty("DocServicePrices")]
        public virtual Service Service { get; set; }
    }
}
