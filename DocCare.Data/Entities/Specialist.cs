﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Specialist
    {
        [Key]
        public int SpecialistId { get; set; }
        [Required]
        [MaxLength(200)]
        public string SpecialistName { get; set; }
        [MaxLength(2000)]
        public string SpecialistDesc { get; set; }
        public bool SpecialistIsActive { get; set; }
        [Required]
        public int HospitalId { get; set; }


        [ForeignKey("HospitalId")]
        [InverseProperty("Specialists")]
        public virtual Hospital Hospital { get; set; }

        public ICollection<Service> Services { get; set; }
    }
}
