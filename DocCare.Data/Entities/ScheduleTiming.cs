﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class ScheduleTiming
    {
        [Key]
        public int ScheduleId { get; set; }
        [Required]
        public TimeSpan ScheduleFrom { get; set; }
        [Required]
        public TimeSpan ScheduleTo { get; set; }
        [Required]
        public int AppointmentsNo { get; set; }
        [Required]
        public long DoctorId { get; set; }
        [Required]
        public int DayId { get; set; }



        [ForeignKey("DoctorId")]
        [InverseProperty("ScheduleTimings")]
        public virtual User Doctor { get; set; }
        [ForeignKey("DayId")]
        [InverseProperty("ScheduleTimings")]
        public virtual Day Day { get; set; }


        public ICollection<Slot> Slots { get; set; }
        //[Required]
        //public int SlotId { get; set; }

        //[ForeignKey("SlotId")]
        //public DoctorSlot Slot { get; set; }
    }
}
