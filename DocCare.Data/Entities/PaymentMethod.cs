﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DocCare.Data.Entities
{
    public class PaymentMethod
    {
        [Key]
        public int PaymentId { get; set; }
        [Required]
        [MaxLength(100)]
        public string PaymentName { get; set; }
        [Required]
        public bool PaymentIsActive { get; set; }

        public ICollection<HospitalPaymentMethod> HospitalPaymentMethods { get; set; }
        public ICollection<Invoice> Invoices { get; set; }
    }
}
