﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Slot
    {
        [Key]
        public long SlotId { get; set; }
        [Required]
        public TimeSpan SlotTimeFrom { get; set; }
        [Required]
        public TimeSpan SlotTimeTo { get; set; }
        [Required]
        public bool SlotIsBooked { get; set; }
        [Required]
        public int ScheduleId { get; set; }


        [ForeignKey("ScheduleId")]
        [InverseProperty("Slots")]
        public virtual ScheduleTiming ScheduleTiming { get; set; }
    }
}
