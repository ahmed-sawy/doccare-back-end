﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Appointment
    {
        [Key]
        public long AppointmentId { get; set; }
        [Required]
        public DateTime AppointmentBookDate { get; set; }
        [Required]
        public DateTime AppointmentDate { get; set; }
        [Required]
        public TimeSpan AppointmentFrom { get; set; }
        [Required]
        public TimeSpan AppointmentTo { get; set; }
        [Required]
        public decimal AppointmentAmount { get; set; }
        [Required]
        public long PatientId { get; set; }
        [Required]
        public long DoctorId { get; set; }


        //[Required]
        //public long SlotId { get; set; }
        //public DoctorSlot Slot { get; set; }

        [ForeignKey("PatientId")]
        [InverseProperty("PatientAppointments")]
        public virtual User Patient { get; set; }
        [ForeignKey("DoctorId")]
        [InverseProperty("DoctorAppointments")]
        public virtual User Doctor { get; set; }


        public ICollection<Invoice> Invoices { get; set; }
    }
}
