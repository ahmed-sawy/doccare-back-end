﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DocCare.Data.Entities
{
    public class State
    {
        [Key]
        public int StatrId { get; set; }
        [Required]
        [MaxLength(100)]
        public string StateName { get; set; }
        [Required]
        public bool StateIsActive { get; set; }
        [Required]
        public int CityId { get; set; }
        public City City { get; set; }
    }
}
