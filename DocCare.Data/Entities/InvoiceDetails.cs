﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class InvoiceDetails
    {
        [Key]
        public long InvServiceId { get; set; }
        [Required]
        public decimal InvServicePrice { get; set; }
        [Required]
        public decimal InvServiceDiscount { get; set; }
        [Required]
        public decimal InvServiceTotalPrice { get; set; }
        [Required]
        public int InvServiceQuntity { get; set; }
        [Required]
        public int ServiceId { get; set; }
        [Required]
        public long InvoiceId { get; set; }


        [ForeignKey("ServiceId")]
        [InverseProperty("InvoiceDetails")]
        public Service Service { get; set; }
        [ForeignKey("InvoiceId")]
        [InverseProperty("InvoiceDetails")]
        public virtual Invoice Invoice { get; set; }
    }
}
