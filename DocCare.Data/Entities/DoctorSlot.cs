﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class DoctorSlot
    {
        [Key]
        public long SlotId { get; set; }
        [Required]
        public TimeSpan SlotTimeFrom { get; set; }
        [Required]
        public TimeSpan SlotTimeTo { get; set; }
        [Required]
        public bool SlotIsBooked { get; set; }
        [Required]
        [ForeignKey("UserId")]
        public long DoctorId { get; set; }
        [Required]
        public int DayId { get; set; }

        public User Doctor { get; set; }
        public Day Day { get; set; }
    }
}
