﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Day
    {
        [Key]
        public int DayId { get; set; }
        [Required]
        [MaxLength(50)]
        public string DayName { get; set; }
        [MaxLength(200)]
        public string DayDesc { get; set; }
        [Required]
        public bool DayIsActive { get; set; }


        public ICollection<ScheduleTiming> ScheduleTimings { get; set; }

    }
}
