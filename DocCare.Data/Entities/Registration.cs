﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Registration
    {
        [Key]
        public int RegistrationId { get; set; }
        [Required]
        [MaxLength(2000)]
        public string Registrations { get; set; }
        [Required]
        public DateTime RegistrationYear { get; set; }
        [Required]
        public long DoctorId { get; set; }


        [ForeignKey("DoctorId")]
        [InverseProperty("Registrations")]
        public virtual User Doctor { get; set; }
    }
}
