﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class SlotDuration
    {
        [Key]
        public int SlotDurationId { get; set; }
        [Required]
        [MaxLength(50)]
        public string SlotDurationName { get; set; }
        [MaxLength(200)]
        public string SlotDurationDesc { get; set; }
        [Required]
        public decimal SlotDurationValue { get; set; }
        [Required]
        public bool SlotDurationIsActive { get; set; }
        public int HospitalId { get; set; }


        [ForeignKey("HospitalId")]
        [InverseProperty("SlotDurations")]
        public virtual Hospital Hospital { get; set; }
    }
}
