﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocCare.Data.Entities
{
    public class Award
    {
        [Key]
        public int AwardId { get; set; }
        [Required]
        [MaxLength(2000)]
        public string Awards { get; set; }
        [Required]
        public DateTime AwardYear { get; set; }
        [Required]
        public long DoctorId { get; set; }



        [ForeignKey("DoctorId")]
        [InverseProperty("Awards")]
        public virtual User Doctor { get; set; }
    }
}
