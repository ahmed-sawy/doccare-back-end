﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DocCare.Data.Migrations
{
    public partial class AddServiceAndDocServicePriceTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Services",
                columns: table => new
                {
                    ServiceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ServiceName = table.Column<string>(maxLength: 200, nullable: false),
                    ServiceDesc = table.Column<string>(maxLength: 2000, nullable: true),
                    ServiceIsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.ServiceId);
                });

            migrationBuilder.CreateTable(
                name: "DocServicePrices",
                columns: table => new
                {
                    DoctorId = table.Column<long>(nullable: false),
                    ServiceId = table.Column<int>(nullable: false),
                    ServicePrice = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocServicePrices", x => new { x.ServiceId, x.DoctorId });
                    table.ForeignKey(
                        name: "FK_DocServicePrices_Users_DoctorId",
                        column: x => x.DoctorId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocServicePrices_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "ServiceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DocServicePrices_DoctorId",
                table: "DocServicePrices",
                column: "DoctorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DocServicePrices");

            migrationBuilder.DropTable(
                name: "Services");
        }
    }
}
