﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DocCare.Data.Migrations
{
    public partial class DocCareInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserCode = table.Column<string>(maxLength: 50, nullable: false),
                    UserFirstName = table.Column<string>(maxLength: 50, nullable: false),
                    UserLastName = table.Column<string>(maxLength: 50, nullable: false),
                    UserDateofBirth = table.Column<DateTime>(nullable: false),
                    UserEmail = table.Column<string>(nullable: true),
                    UserPhone = table.Column<string>(nullable: false),
                    UserPass = table.Column<string>(nullable: false),
                    UserIsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Registrations",
                columns: table => new
                {
                    RegistrationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Registrations = table.Column<string>(maxLength: 2000, nullable: false),
                    RegistrationYear = table.Column<DateTime>(nullable: false),
                    DoctorId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Registrations", x => x.RegistrationId);
                    table.ForeignKey(
                        name: "FK_Registrations_Users_DoctorId",
                        column: x => x.DoctorId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ScheduleTimings",
                columns: table => new
                {
                    ScheduleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ScheduleFrom = table.Column<TimeSpan>(nullable: false),
                    ScheduleTo = table.Column<TimeSpan>(nullable: false),
                    AppointmentsNo = table.Column<int>(nullable: false),
                    DoctorId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduleTimings", x => x.ScheduleId);
                    table.ForeignKey(
                        name: "FK_ScheduleTimings_Users_DoctorId",
                        column: x => x.DoctorId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Registrations_DoctorId",
                table: "Registrations",
                column: "DoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_ScheduleTimings_DoctorId",
                table: "ScheduleTimings",
                column: "DoctorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Registrations");

            migrationBuilder.DropTable(
                name: "ScheduleTimings");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
