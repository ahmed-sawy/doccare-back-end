﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DocCare.Data.Migrations
{
    public partial class AddRelationBetweenUsersAndBloodGeoups : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BloodGroupId",
                table: "Users",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Users_BloodGroupId",
                table: "Users",
                column: "BloodGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_BloodGroups_BloodGroupId",
                table: "Users",
                column: "BloodGroupId",
                principalTable: "BloodGroups",
                principalColumn: "GroupId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_BloodGroups_BloodGroupId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_BloodGroupId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "BloodGroupId",
                table: "Users");
        }
    }
}
