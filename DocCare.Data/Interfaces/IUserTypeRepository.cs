﻿using DocCare.Data.Entities;

namespace DocCare.Data.Interfaces
{
    public interface IUserTypeRepository : IGenericRepository<UserType>
    {
    }
}
