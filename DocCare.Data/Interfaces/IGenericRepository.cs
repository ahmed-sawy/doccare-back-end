﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DocCare.Data.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        TEntity GetByID(long ID);
        List<TEntity> GetAll();
        List<TEntity> Search(Expression<Func<TEntity, bool>> predicate);

        void Update(TEntity oEntity);

        void Add(TEntity oEntity);
        void AddRange(List<TEntity> lEntities);

        void Remove(TEntity oEntity);
        void RemoveRange(List<TEntity> lEntities);
    }
}
