﻿using DocCare.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Data.Interfaces
{
    public interface IAppointmentRepository : IGenericRepository<Appointment>
    {
    }
}
