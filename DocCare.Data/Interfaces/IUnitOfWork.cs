﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Data.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IDayRepository Days { get; }
        IHospitalRepository Hospitals { get; }
        ISlotRepository Slots { get; }
        ISpecialistRepository Specialists { get; }
        IUserRepository Users { get; }
        IAppointmentRepository Appointments { get; }
        IAwardRepository Awards { get; }
        IDocServicePriceRepository DocServicePrices { get; }
        IDoctorSlotRepository DoctorSlots { get; }
        IEducationRepository Educations { get; }
        IExperienceRepository Experiences { get; }
        IFavoriteRepository Favorites { get; }
        IMembershipRepository Memberships { get; }
        IRegistationRepository Registations { get; }
        IServiceRepository Services { get; }
        IUserTypeRepository UserTypes { get; }
        ISocialMediaRepository SocialMedias { get; }


        int Complete();
    }
}
