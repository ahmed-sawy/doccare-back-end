﻿using DocCare.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DocCare.Data.Interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
        User LoginUser(string UserEmail, string UserPass);
        User GetByIdWithRelatedData(long ID);
        User GetByIdWithScheduleTiming(long ID);
        List<User> SearchUsersWithRelatedData(Expression<Func<User, bool>> predicate);

        //List<User> GetByUsernameStored(string username);
    }
}
