﻿using DocCare.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocCare.Data.Interfaces
{
    public interface ISocialMediaRepository : IGenericRepository<SocialMedia>
    {
        SocialMedia GetSocialMediaByDoctorId(long doctorId);
    }
}
